#!/usr/bin/python3
import numpy as np
from scipy.sparse import dok_matrix,lil_matrix,linalg,csr_matrix,csc_matrix
import time
t1=time.time()
N=1000

def mkidx(i,j):
    global N
    return i*N+j

def getidx(n):
    global N
    return (n//N,n%N)

def La():
    global N
    N2=N*N
    sa=dok_matrix((N2, N2))
    for i in range(N-1):
        for j in range(N):
            sa[mkidx(i+1,j),mkidx(i,j)  ] += 1
            sa[mkidx(i,j),  mkidx(i+1,j)] +=1
    for i in range(N):
        for j in range(N-1):
            sa[mkidx(i,j+1),mkidx(i,j)  ] +=1
            sa[mkidx(i,j),  mkidx(i,j+1)] +=1
    for j in range(N):
        sa[mkidx(0,j), mkidx(1,j)] += 1 
        sa[mkidx(j,0), mkidx(j,1)] += 1 
    for i in range(N):
        for j in range(N):
            zzz=4 if i < N-1 and j < N-1 else 3 if i < N-1 or j < N-1 else 2
            sa[mkidx(i,j), mkidx(i,j)] -= zzz
    # introduce a small error so on the boundary so that the matrix
    # is no longer singular        
    sa[N2-1, N2-1] += 0.01        
    return sa
 
Is=np.zeros((N*N,))
Is[mkidx(0,0)]=2.0
irq=-2.0/(8*(N-1))
for i in range(N):
    Is[mkidx(i,N-1)]=irq
    Is[mkidx(N-1,i)]=irq  
s=La()
x=linalg.spsolve(csc_matrix(s),Is)
print("original midle potential",x[0])
x -= x[0]
t2=time.time()
dt=t2-t1
r11=x[mkidx(1,1)]
r10=x[mkidx(0,1)]
print(f"R11={r11},  R01=R10={r10}")
print("r11*pi*0.5=",r11*0.5*np.pi,"  r11err=",r11*0.5*np.pi-1)
print(f"runtime={dt:0.03f} seconds")
    
