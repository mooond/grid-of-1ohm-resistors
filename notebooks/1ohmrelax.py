#!/usr/bin/python3
import numpy as np
import time
t1=time.time()
N=500

def fourshiftq(rin):
    rout=np.zeros(np.shape(rin))
    rout[1:,:] += rin[:-1,:]
    rout[:-1,:] += rin[1:,:]
    rout[:,1:] += rin[:,:-1]
    rout[:,:-1] += rin[:,1:]
    rout[0,:] += rin[1,:]
    rout[:,0] += rin[:,1]
    return rout


def Lq(rin):
    ro=fourshiftq(rin)
    ro = (ro+Iq)  *rzq
    ro -= ro[0,0]
    return ro

zq=np.ones((N,N))
zzq=fourshiftq(zq)
rzq=1/zzq

Iq=np.zeros((N,N))
Iq[0,0]=-2
irq=2.0/(8*(N-1))
Iq[-1,:]=irq
Iq[:,-1]=irq    

r1=np.zeros((N,N))

for i in range(50000):
    rn=Lq(r1)
    r1 += (rn-r1)*0.95
    r1 -= r1[0,0]

err1=(rn-r1)*(rn-r1)
errsum=np.sqrt(np.sum(err1,axis=(0,1))/N/N)

t2=time.time()
dt=t2-t1

r11=r1[1,1]
r10=r1[0,1]
print("errsum=",errsum)
print(f"R11={r11},  R01=R10={r10}")
print("r11*pi*0.5=",r11*0.5*np.pi,"  r11err=",r11*0.5*np.pi-1)
print(f"runtime={dt:0.03f} seconds")
    
