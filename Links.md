

## Generating Functions:

* Good and Brief Intro:
  MIT Lecture Notes "Generating Functions" (Michel Goemans): 
  https://math.mit.edu/~goemans/18310S15/generating-function-notes.pdf

* From: "An Introduction to the Analysis of Algorithms" (by:  Robert Sedgewick and Phillipe Flajolet)
  https://aofa.cs.princeton.edu/30gf/

* A short table of generating functions and related formulas (Robert M. Ziff)
  Michigan Center for Theoretical Physics and Department of Chemical Engineering
  University of Michigan, Ann Arbor, MI 48109-2136 USA:
  https://www.researchgate.net/publication/48200916_A_short_table_of_generating_functions_and_related_formulas

* Oscar Levin
  University of Northern Colorado, From: "Discrete Mathematics"
  https://math.libretexts.org/Bookshelves/Combinatorics_and_Discrete_Mathematics/Discrete_Mathematics_(Levin)/5%3A_Additional_Topics/5.1%3A_Generating_Functions



