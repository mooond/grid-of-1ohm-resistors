from random import random
N=10_000_000
cnt=0
for i in range(N):
   x=random()
   y=random()
   cnt += 1 if x*x+y*y <= 1 else 0
print("pi is about",4.0*cnt/N)