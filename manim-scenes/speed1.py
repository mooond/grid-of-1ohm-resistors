#!/usr/bin/env python3
import numpy as np
from time import time

t1=time()
rnd1=[]

for i in range(1000_000):
   r=np.random.rand()
   rnd1.append(r*r)

t2=time()

rnd2=np.random.rand(1000_000)
rnd2=rnd2*rnd2

t3=time()

print(f"time for loop {t2-t1:0.5f} seconds")
print(f"time for rand {t3-t2:0.5f} seconds")
print(f"speedup {(t2-t1)/(t3-t2):0.1f} times")
   