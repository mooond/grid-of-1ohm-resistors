from manim import *
import numpy as np
#from manim.imports import * 
from manim_physics import *
import lcapy

def myscale(obj,fact):
  # scale around origin
  c=obj.get_center()
  obj.scale(fact)
  obj.shift((fact-1)*c)
  return obj

def mkcoordmn(m,n):
  if m < 0:
    coord=f"m-{abs(m)},"
  elif m > 0:
    coord=f"m+{abs(m)},"
  else:
    coord=f"m,"  
  if n < 0:
    coord += f"n-{abs(n)}"
  elif n > 0:
    coord += f"n+{abs(n)}"
  else:
    coord += f"n"  
  coord=coord.replace('-',r'‒') #unicode minus
  return coord

def mkcoord(m,n):
  if m < 0:
    coord=f"-{abs(m)},"
  elif m > 0:
    coord=f"{abs(m)},"
  else:
    coord=f"0,"  
  if n < 0:
    coord += f"-{abs(n)}"
  elif n > 0:
    coord += f"{abs(n)}"
  else:
    coord += f"0"  
  coord=coord.replace('-',r'‒') #unicode minus
  return coord


class FunSequence(VGroup):
    def __init__(self,seq=None,fun=None,start=-2,stop=12,scale=0.5,xscale=0.7):
        super().__init__()
        self.fun=fun
        self.seq=seq
        self.scale=scale
        self.xscale=xscale
        if not seq:
          self.seq=[]
          seq=self.seq
        if fun:
          for i in range(start,stop,1):
            self.seq.append(fun(i))
        print("seq start=",start,"stop=",stop,"seq=",seq)
        self.offset=-2
        self.baseline=Line([start+self.offset,0,0],[stop+self.offset,0,0],color=GREEN)
        self.add(self.baseline)
        for i in range(start,stop,1):
          color = WHITE if i >= 0 else RED
          self.add(Dot([xscale*i+self.offset,0,0],color=color))
          self.add(Text(str(i),font_size=12,color=color).move_to([xscale*i+self.offset+0.1,0.1,0]))
          val=seq[i-start]
          if val > 0 or i >= 0:
            dt=Dot([xscale*i+self.offset,val*scale,0],color=GREEN_E)
            self.add(dt)
            self.add(Line([xscale*i+self.offset,0,0],dt.get_bottom(),stroke_width=0.5))
            sval=str(round(val,3))
            fsize=24
            if len(sval)>2:
              fsize=16
              if len(sval)>4:
                fsize=12
            self.add(Text(sval,font_size=fsize).move_to([xscale*i+self.offset+0.2,val*scale+0.2,0]))
  

class MultiMeter(VGroup):
   def __init__(self,pos=None,red=None,black=None,rtred=35,rtblack=-35,disp=None):
        super().__init__()
        self.pos=pos
        mm = SVGMobject("multimeter.svg",stroke_width=0.0).scale(1.5)
        rt=  SVGMobject("redtip.svg",stroke_width=0.0)
        bt=  SVGMobject("blacktip.svg",stroke_width=0.0)
        self.mm=mm
        if pos:
          mm.move_to(pos)
        self.add(mm)
        if red:
          rt.move_to(red)
          rt.shift(rt.height*DOWN*0.5)
          rtarget=rt.get_top() + [rt.height*np.sin(rtred*DEGREES),-rt.height*np.cos(rtred*DEGREES),0]
          rt.rotate(rtred*DEGREES,about_point=rt.get_top())
          #t1=red.get_top()
          #delta=rt.get_top()-t1
          #rt.move_to(UP*delta)
          self.add(rt)
          rstart=mm.get_bottom()+RIGHT*0.6+UP*0.57
          bezier = CubicBezier(rstart, rstart + 1 * RIGHT, rtarget + [rt.height*np.sin(rtred*DEGREES),-rt.height*np.cos(rtred*DEGREES),0] , rtarget, stroke_width=6)
          bezier.color="#A00000"
          self.add(bezier)
          
        if black:
          bt.move_to(black)
          bt.shift(bt.height*DOWN*0.5)
          btarget=bt.get_top() + [bt.height*np.sin(rtblack*DEGREES),-bt.height*np.cos(rtblack*DEGREES),0]
          bt.rotate(rtblack*DEGREES,about_point=bt.get_top())
          #t2=black.get_top()
          #delta=bt.get_top()-t2
          #bt.move_to(UP*delta)  
          self.add(bt)
          bstart=mm.get_bottom()+RIGHT*0.6+UP*0.32
          bezier = CubicBezier(bstart, bstart - 1 * UP, btarget + [bt.height*np.sin(rtblack*DEGREES),-bt.height*np.cos(rtblack*DEGREES),0] , btarget, stroke_width=6)
          bezier.color="#000000"
          self.add(bezier)
        if disp:
          disptxt=Text(disp,font="DSEG14 Classic",font_size=20,color=BLACK)
          disptxt.move_to(mm.get_top() + DOWN*0.4+LEFT*0.15)
          self.disptxt=disptxt
          self.add(disptxt)
          
   def update_text(self,newtext):
      disptxt=self.disptxt
      newdisp=Text(newtext,font="DSEG14 Classic",font_size=20,color=BLACK)
      newdisp.move_to(self.mm.get_top() + DOWN*0.4+LEFT*0.15)
      return Transform(disptxt,newdisp)

class Resistor(VGroup):
   def __init__(self,txt=None):
        super().__init__()
        self.txt=txt
        self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        self.body=Rectangle(height=0.5, width=0.15, stroke_width=2)
        self.body.shift(2*UP/4)
        for d in self.dots:
          self.add(d)
        self.add(self.body)
        if self.txt:
          self.tex = Tex(txt, font_size=12)
          self.tex.shift(UP/2+RIGHT/5)
          self.add(self.tex)  
        self.add(Line(self.dots[0],UP/4,stroke_width=2))
        self.add(Line(UP*3/4,self.dots[1],stroke_width=2))

class Battery(VGroup):
   def __init__(self,txt=None):
        super().__init__()
        self.txt=txt
        self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        for d in self.dots:
          self.add(d)
        self.add(Line(self.dots[0],UP*2/5,stroke_width=2))
        self.add(Line(UP*3/5,self.dots[1],stroke_width=2))
        lplus=Line(UP*2/5+LEFT*0.2,UP*2/5-LEFT*0.2,stroke_width=2)
        self.add(lplus)
        self.add(Line(UP*3/5+LEFT*0.3,UP*3/5-LEFT*0.3,stroke_width=2))
        self.add(Text("+",font_size=18).move_to(UP*3.3/5+LEFT*0.2))
        self.add(Text("-",font_size=18).move_to(UP*1.7/5+LEFT*0.2))
        if self.txt:
          self.tex = Tex(txt, font_size=24)
          self.tex.shift(UP/2) #
          self.tex.next_to(lplus,LEFT)
          self.add(self.tex)  


class CurrentSource(VGroup):
   def __init__(self,txt=None):
        super().__init__()
        self.txt=txt
        self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        for d in self.dots:
          self.add(d)
        circ=Circle(radius=1.0/4.0,color=WHITE,stroke_width=2).shift(UP*1.0/2.0)
        self.add(circ)
        self.add(Line(self.dots[0],UP*1/4,stroke_width=2))
        self.add(Line(UP*3/4,self.dots[1],stroke_width=2))
        self.add(Arrow( [0,1.3/4.0,0], [0,2.7/4,0] ))
        if self.txt:
          self.tex = Tex(txt, font_size=24)
          self.tex.shift(UP/2).next_to(circ,LEFT)
          self.add(self.tex)  


class Short(VGroup):
   def __init__(self,replace=None):
        super().__init__()
        if replace:
          self.dots=replace.dots.copy()
        else:
          self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        for d in self.dots:
          self.add(d)
        self.add(Line(self.dots[0],self.dots[1],stroke_width=2))


class scene3s1(MovingCameraScene):
    def wait_till(self,n):
      ns=n
      ts=self.timestamp
      if isinstance(n,str):
        fval=[1,60,60]
        nfields=n.split(":")
        n=sum([fval*int(nf) for fval,nf in zip(fval,nfields[::-1])])
        print("waitting till",n,ns)
      tdelta=n-self.renderer.time-ts
      if tdelta < 0:
        print("============= BEHIND SCHEDULE ==============")
      elif tdelta > 0:  
        self.wait(tdelta)
        
    def construct(self):
      myTemplate = TexTemplate()
      myTemplate.add_to_preamble(r"\usepackage{trfsigns}")
      self.timestamp=0
      self.camera.background_color = '#302800'
      # r1 = drawgrid(10)
      # r1.draw(style='european',scale=3,filename="tmp0s1.svg",color='white')
      chap1=Tex(r"{\LARGE Chapter III} \\ Finding a Solution for the Infinite Grid").move_to([0, 3, 0])
      self.play(Write(chap1))
      self.add(chap1)
      self.wait_till("00:14")
      tx1=Tex(r"$  F_{m,n} \laplace f(x,y) $ ",tex_template=myTemplate).move_to([-4.8,2,0])
      self.add(tx1)
      self.wait_till("00:32")
      tx2=Tex(r"$  f(x,y)= F_{0,0} + F_{1,0} x + F_{0,1} y + F_{1,1} x y + F_{2,0} x^2 + F_{2,1} x^2 y + \ldots ..  $ \\ $  \ldots + F_{m,n}x^m y^n + \ldots  $ ").next_to(tx1,DOWN).align_to(tx1,LEFT)
      self.add(tx2)
      self.wait_till("01:00")
      tx3=Tex(r"$ \mathbf{L}F = -4 F_{m,n}+F_{m+1,n}+F_{m-1,n}+F_{m,n+1}+F_{m,n-1} = I_{m,n} \delta(x,y,0)$").next_to(tx2,DOWN).align_to(tx2,LEFT)
      self.add(tx3)
      self.wait_till("01:22")
      tx4=Tex(r"$  -4 f + x^{-1} \cdot f + x \cdot f + y^{-1} \cdot f  + y \cdot f  = 1 $").next_to(tx3,DOWN).align_to(tx2,LEFT)
      self.add(tx4)
      self.wait(5)
      tx5=Tex(r"$ f(x,y) \cdot ( -4 + \frac{1}{x}  + x  + \frac{1}{y} + y )   = 1 $").next_to(tx4,DOWN).align_to(tx2,LEFT)
      self.add(tx5)
      self.wait_till("01:41")
      #
      #
      self.clear()
      tx1=Tex(r"$ f(x,y) \cdot ( -4 + \frac{1}{x}  + x  + \frac{1}{y} + y )   = 1 $").move_to([-3,3,0])
      self.add(tx1)
      self.wait_till("01:48")
      tx1=Tex(r"$ f(x,y) = \frac{1}{ -4 + \frac{1}{x}  + x  + \frac{1}{y} + y } $",font_size=120).next_to(tx1,DOWN).align_to(tx1,LEFT)
      self.add(tx1)
      self.wait_till("01:55")
      nox=VGroup()
      nox.add(Line([-4,2,0],[4,-2,0],color=RED))
      nox.add(Line([-4,-2,0],[4,2,0],color=RED))
      nox.move_to(tx1)
      self.add(nox)
      self.wait_till("02:04")
      self.add(Text("Why is this wrong?").move_to([0,-3,0]))
      self.wait_till("02:37")
      ####
      #
      #
      self.clear()
      numplane=NumberPlane(x_range=(-3, 9, 1), y_range=(-3, 4, 1))
      self.add(numplane)
      q1=Rectangle(width=12.0, height=6.0,color=YELLOW).align_to(numplane.coords_to_point(0,0),LEFT+DOWN)
      q1.set_fill(TEAL_E,opacity=0.4)
      self.add(q1)
      for m in range(-3,4,1):
        for n in range(-3,9,1):#
          coord=mkcoord(m,n)
          color=RED if m>=0 and n>=0 else WHITE
          dt=Dot(numplane.coords_to_point(n,m),color=color,radius=0.07)
          self.add(Tex(r"$ F_{" + coord + "} $",font_size=20).align_to([0.1,0.1,0]+numplane.coords_to_point(n,m),LEFT+DOWN))
          self.add(dt)
      #self.add(q1)    
      self.wait(5)

      self.clear()
      c1=Circle(radius=3)
      c0=Dot(color=BLUE,radius=0.3)
      ax=Axes(x_range=(-4,4),y_range=(-4,4),x_length =8,y_length=8,axis_config={"include_numbers": True})
      ax.add(c0)
      self.add(ax)
      self.wait_till("03:17")
      tx1=Tex(r"$ \nabla^2 F(\vec x) =\delta(\vec x) $").move_to([-5,3,0])
      self.add(tx1)
      self.wait_till("03:48")
      na=24
      for k in range(na):
        v1=RIGHT*np.cos(k*2*np.pi/na)+UP*np.sin(k*2*np.pi/na)
        self.add(Arrow(v1*0.5,v1*0.9))
        self.add(Arrow(v1*3.5,v1*4.2))
      self.wait(3)
      ax.add(c1)
      self.add(Text("R").align_to([3.1,0.1,0],DOWN+LEFT))
      #self.add(q1)    
      self.wait_till("04:06")
      tx2=Tex(r"$ \vec \nabla \vec E(\vec x) =\delta(\vec x) $").move_to([5,3,0])
      self.add(tx2)
      self.wait_till("04:36")
      tx3=Tex(r"3D: \\ $ E = \frac{1}{4 \pi R^2} $").next_to(tx2,DOWN).align_to(tx2,RIGHT)
      self.add(tx3)
      self.wait_till("04:54")
      tx4=Tex(r"$ F = \frac{-1}{4 \pi |R|} $").next_to(tx3,DOWN).align_to(tx2,RIGHT)
      self.add(tx4)
      self.wait_till("05:15")
      self.remove(tx3,tx4)
      tx3=Tex(r"2D: \\ $ E = \frac{1}{2 \pi R} $").next_to(tx2,DOWN).align_to(tx2,RIGHT)
      self.add(tx3)
      self.wait_till("05:28")
      tx4=Tex(r"$ F = \frac{ln(|R|)}{2 \pi } $").next_to(tx3,DOWN).align_to(tx2,RIGHT)
      self.add(tx4)
      self.wait_till("05:52")
      #
      #
      #
      self.clear()
      tx1=Text("Green's Function",font_size=64).move_to([0,3.5,0])
      self.add(tx1)
      tx2=Tex(r"$ \nabla^2 G(\vec x) =\delta(\vec x) $").move_to([-5,2.5,0])
      self.add(tx2)
      self.wait_till("06:20")
      tx3=Tex(r"$ \mathbf{L} G =\delta $").next_to(tx2,DOWN).align_to(tx2,LEFT)
      self.add(tx3)
      self.wait_till("07:03")
      self.clear()
      #
      #
      numplane=NumberPlane(x_range=(-4, 4, 1), y_range=(-4, 4, 1)).shift(LEFT*2)
      self.add(numplane)
      for m in range(-4,4,1):
        for n in range(-4,4,1):#
          coord=mkcoord(m,n)
          color=WHITE #RED if m>=0 and n>=0 else WHITE
          dt=Dot(numplane.coords_to_point(n,m),color=color,radius=0.07)
          self.add(Tex(r"$ F_{" + coord + "} $",font_size=20).align_to([0.1,0.1,0]+numplane.coords_to_point(n,m),LEFT+DOWN))
          self.add(dt)
      #self.add(q1)    
      self.wait_till("07:17")
      q2=Rectangle(width=5.0, height=5.0,color=PURPLE).align_to(numplane.coords_to_point(-2.5,-2.5),LEFT+DOWN)
      q2.set_fill(TEAL_E,opacity=0.2)
      self.add(q2)
      self.wait_till("07:46")
      for i in range(-2,3):
        self.add(Arrow(numplane.coords_to_point(i,2),numplane.coords_to_point(i,3),color=RED))
      q1=RoundedRectangle(corner_radius=0.3,width=4.6, height=0.8,color=YELLOW).align_to(numplane.coords_to_point(-2.4,2.6),LEFT+DOWN)
      q1.set_fill(TEAL_E,opacity=0.2)
      self.add(q1)
      self.wait_till("07:53")
      q3=RoundedRectangle(corner_radius=0.3,width=4.6, height=0.8,color=BLUE).align_to(numplane.coords_to_point(-2.4,1.6),LEFT+DOWN)
      q3.set_fill(TEAL_E,opacity=0.2)
      self.add(q3)
      tx2=Tex(r"$ \sum\limits_{m=2} F_{mn} - \sum\limits_{m=3} F_{mn} = I_{in}/4 $",font_size=36).move_to([4,3,0])
      self.add(tx2) 
      self.wait_till("08:16")
      #
      self.clear()
      tx1=Text("Alternatives",font_size=64).move_to([0,3.5,0])
      self.add(tx1)
      self.wait_till("08:21")
      tx2=Tex(r"$ F_n=A \cdot \lambda_1 ^n $").next_to(tx1,DOWN).shift(LEFT*4)
      self.add(tx2)
      self.wait_till("08:47")
      tx3=Tex(r"$ F_n=A \cdot e^ { n \cdot ln(\lambda_1)} = A \cdot e^ { n \cdot c_1}  =  $").next_to(tx2,DOWN).align_to(tx2,LEFT)
      self.add(tx3)
      self.wait_till("08:59")
      tx4=Tex(r"$ F_{m,n} = A \cdot e^{ n \cdot c_1} \cdot e^{ m \cdot c_2}  =  A \cdot e^{n\cdot c_1 + m\cdot c_2} $").next_to(tx3,DOWN).align_to(tx2,LEFT)
      self.add(tx4)
      self.wait_till("09:20")
      tx5=Tex(r"Shifting from $m$ to $m+1$ gives").next_to(tx4,DOWN).align_to(tx2,LEFT)
      self.add(tx5)
      self.wait(7)
      tx6=Tex(r"$ F_{m+1,n} = A \cdot e^{n\cdot c_1 + (m+1)\cdot c_2} = F_{m,n} \cdot e^{c_2} = F_{m,n} \cdot \lambda_2 $").next_to(tx5,DOWN).align_to(tx2,LEFT)
      self.add(tx6)
      self.wait_till("09:50")
      tx7=Tex(r"$ \mathbf{L} F = F \cdot ( -4 + \lambda_2 +\lambda_1 + \frac{1}{\lambda_1} + \frac{1}{\lambda_2} ) $").next_to(tx6,DOWN).align_to(tx2,LEFT)
      self.add(tx7)
      self.wait_till("10:15")
      #
      #
      self.clear()
      tx1=Text("Periodic Functions",font_size=64).move_to([0,3.5,0])
      self.add(tx1)
      self.wait(8)
      tx2=Tex(r"$ c_1 = j \cdot \beta $  \hspace{1cm} $ c_2= j \cdot \alpha $").next_to(tx1,DOWN).shift(LEFT*4)
      self.add(tx2)
      tx3=Tex(r"$ F_{m,n} = A \cdot e^{j \cdot m \cdot \alpha  + j \cdot n \cdot \beta } $").next_to(tx2,DOWN).align_to(tx2,LEFT)
      self.add(tx3)
      self.wait_till("11:10")
      tx4=Tex(r"$ F_{m+1,n} + F_{m-1,n} = Ae^{j n \beta}  \cdot ( e^{j \cdot (m+1)\alpha}  + e^{j \cdot (m-1) \alpha } ) $").next_to(tx3,DOWN).align_to(tx2,LEFT)
      self.add(tx4)
      self.wait_till("11:26")
      tx5=Tex(r"$ e^{j\alpha} = cos(\alpha) + j\cdot sin(\alpha) $").next_to(tx4,DOWN).align_to(tx2,LEFT)
      self.add(tx5)
      self.wait(12)
      tx6=Tex(r"$ F_{m+1,n} + F_{m-1,n} = F_{m,n}  \cdot  ( e^{j \cdot \alpha}  + e^{-j \cdot \alpha } )  = 2 cos(\alpha) \cdot F_{m,n} $").next_to(tx5,DOWN).align_to(tx2,LEFT)
      self.add(tx6)
      self.wait_till("12:09")
      tx7=Tex(r"$ \mathbf{L} F_E = F_E  \cdot  ( -4 + 2 cos(\alpha) +2 cos(\beta))  = \lambda F_E  $").next_to(tx6,DOWN).align_to(tx2,LEFT)
      self.add(tx7)
      self.wait_till("12:42")
      tx8=Tex(r"$ F_E  = A \cdot e^{j m \alpha  + j n \beta } $  \ldots Eigenfunctions").next_to(tx7,DOWN).align_to(tx2,LEFT)
      self.add(tx8)
      tx9=Tex(r"$ \lambda = -4 + 2 cos(\alpha) + 2 cos(\beta) $ \ldots Eigenvalues   ").next_to(tx8,DOWN).align_to(tx2,LEFT)
      self.add(tx9)
      self.wait_till("13:19")
      #
      #
      # stiching the 
      self.clear()
      tx1=Text("Stiching Eigenfunctions together",font_size=48).move_to([0,3.5,0])
      self.add(tx1)
      self.wait_till("13:27")
      tx1=Tex(r"$ F_E  = A \cdot e^{j m \alpha  + j n \beta } $  \ldots Eigenfunctions").next_to(tx1,DOWN).shift(LEFT*3)
      self.add(tx1)
      tx2=Tex(r"$ \lambda = -4 + 2 cos(\pm \alpha) + 2 cos(\pm \beta) $ \ldots Eigenvalues ").next_to(tx2,DOWN).align_to(tx2,LEFT)
      self.add(tx2)
      self.wait_till("14:29")
      tx3=Tex(r"$ F_E =  cos(m \alpha)\cos(n \beta) $ \ldots better Eigenfunctions ").next_to(tx2,DOWN).align_to(tx2,LEFT)
      self.add(tx3)
      self.wait_till("15:11")
      # stiching the 
      self.clear()
      tx1=Text("Superposition of Eigenfunctions",font_size=48).move_to([0,3.5,0])
      self.add(tx1)
      self.wait_till("15:20")
      tx1=Tex(r"$ F_{m,n} = k_1 F_{E_1} + k_2 F_{E_2} = k_1 cos(m \alpha_1)\cos(n \beta_1 ) + $ \\ \hspace{2cm} $  +  k_2 cos(m \alpha_2)\cos(n \beta_2 )  $").next_to(tx1,DOWN).align_to(tx2,LEFT)
      self.add(tx1)
      tx2=Tex(r"$ \lambda_1 = -4 + 2 cos(\alpha_1) + 2 cos(\beta_1) $").next_to(tx2,DOWN).align_to(tx2,LEFT)
      self.add(tx2)
      tx3=Tex(r"$ \lambda_2 = -4 + 2 cos(\alpha_2) + 2 cos(\beta_2) $").next_to(tx3,DOWN).align_to(tx2,LEFT)
      self.add(tx2,tx3)
      self.wait_till("16:01")
      tx4=Tex(r"$ \mathbf{L} (k_1 F_{E_1} + k_2 F_{E_2}) = \lambda_1 k_1 F_{E_1} + \lambda_2 k_2 F_{E_2}$").next_to(tx3,DOWN).align_to(tx2,LEFT)
      self.add(tx4)
      self.wait_till("16:32")
      # infinitly many
      self.clear()
      tx1=Text("Adding Infinitely Many of Eigenfunctions",font_size=48).move_to([0,3.5,0])
      self.add(tx1)
      self.wait_till("16:46")
      tx2=Tex(r"$ K_{m,n} =  \frac{1}{4 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} k(\alpha,\beta) \cdot cos(m \alpha)\cos(n \beta) \, d \alpha \, d \beta$ ").next_to(tx1,DOWN).align_to(tx2,LEFT)
      self.add(tx2)
      self.wait_till("18:25")
      tx3=Tex(r"$ K_{m,n} = K(m,n) $ is the 2D Fourier Series of \\ the  Periodic Function $ k(\alpha,\beta) $").next_to(tx2,DOWN).align_to(tx2,LEFT) 
      self.add(tx3)
      self.wait_till("20:53")
      tx3=Tex(r"$ \mathbf{L} K_{m,n} = I_{m,n} $ ").next_to(tx3,DOWN).align_to(tx2,LEFT) 
      self.add(tx3)
      self.wait_till("22:05")
      tx3=Tex(r"$ k(\alpha,\beta) \cdot ( -4 +2cos(\alpha) +2cos(\beta)) = i(\alpha,\beta) $ ").next_to(tx3,DOWN).align_to(tx2,LEFT) 
      self.add(tx3)
      self.wait_till("22:47")
      tx4=Tex(r"$ k(\alpha,\beta) = \frac{i(\alpha,\beta)}{ -4 +2cos(\alpha) +2cos(\beta)} $ ").next_to(tx3,DOWN).align_to(tx2,LEFT) 
      self.add(tx4)
      self.wait_till("23:04")
      #
      #
      self.clear()
      tx1=Text("Switching Integral with the Operation",font_size=40).move_to([0,3.5,0])
      self.add(tx1)
      self.wait_till("23:11")
      tx2=Tex(r"$ \mathbf{L} K =  \mathbf{L} \iint k(\alpha,\beta) F_{E_{\alpha,\beta}} \, dA  = $").next_to(tx1,DOWN).align_to(tx2,LEFT)
      self.add(tx2)
      self.wait_till("23:28")
      tx3=Tex(r" $ = \iint k(\alpha,\beta) \mathbf{L} F_E \, d A = \iint \lambda_{\alpha,\beta} k(\alpha,\beta) F_E \, dA $  ").next_to(tx2,DOWN).align_to(tx2,LEFT).shift(RIGHT)
      self.add(tx3)
      self.wait_till("24:05")
      #
      #
      self.clear()
      tx1=Text("The Righthand Side",font_size=48).move_to([0,3.5,0])
      self.add(tx1)
      self.wait(5)
      tx2=Tex(r"$ I_{m,n} = \Bigl\{^{1 \, \forall  m=n=0}_{0 \, else} $").next_to(tx1,DOWN).align_to(tx2,LEFT)
      self.add(tx2)
      self.wait_till("24:26")
      tx3=Tex(r" $ i(\alpha,\beta) = 1 $").next_to(tx3,DOWN).align_to(tx2,LEFT)
      self.add(tx3)
      self.wait_till("24:46")
      #
      self.clear()
      tx1=Text("The Green's Function",font_size=48).move_to([0,3.5,0])
      self.add(tx1)
      self.wait_till("24:53")
      tx2=Tex(r"$ k(\alpha,\beta) = \frac{1}{ -4 +2cos(\alpha) +2cos(\beta)}  $ ").next_to(tx2,DOWN).align_to(tx2,LEFT) 
      self.add(tx2)
      self.wait_till("25:19")
      tx3=Tex(r"$ K_{m,n} =  \frac{1}{8 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{cos(m\alpha) cos(n \beta)}{ -2 + cos(\alpha) + cos(\beta)} \cdot d \alpha \, d \beta$").next_to(tx2,DOWN).align_to(tx2,LEFT)
      self.add(tx3)
      self.wait_till("26:25")
      tx4=Tex(r"$ G_{m,n} =  \frac{1}{8 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{cos(m \alpha) cos(n \beta) -1 }{ -2 + cos(\alpha) + cos(\beta)} \cdot d \alpha \, d \beta$").next_to(tx3,DOWN).align_to(tx2,LEFT)
      self.add(tx4)
      self.wait_till("27:37")
      #
      #
      self.clear()
      tx1=Text("The Resistance between points",font_size=48).move_to([0,3.5,0])
      self.add(tx1)
      self.wait_till("27:51")
      tx2=Tex(r"$ G_{m,n} =  \frac{1}{8 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{cos(m \alpha) cos(n \beta) -1 }{ -2 + cos(\alpha) + cos(\beta)} \cdot d \alpha \, d \beta$").next_to(tx1,DOWN).align_to(tx2,LEFT)
      self.add(tx2)
      self.wait_till("28:21")
      tx3=Tex(r"$ R_{m,n} =  \frac{1}{4 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{1- cos(m \alpha) cos(n \beta) }{ 2 - cos(\alpha) - cos(\beta)} \cdot d \alpha \, d \beta$").next_to(tx2,DOWN).align_to(tx2,LEFT)
      self.add(tx3)
      self.wait_till("28:57")
      #
      self.clear()
      tx1=Text("Numeric Evaluation",font_size=48).move_to([0,3.5,0])
      np1=oblue=ImageMobject('npginteg.png').scale(1.5)
      self.play(FadeIn(np1))
      self.add(np1)
      self.wait_till("29:27")
      self.remove(np1)
      np2=oblue=ImageMobject('npginteg2.png').scale(2.5)
      self.play(FadeIn(np2))
      self.add(np2)
      self.wait_till("29:55")
      self.clear()
      imax=11
      jmax=13
      for d in range(2):
        for i in range(imax+1):
          for j in range(jmax+1):
            R1=Resistor(r"$1\Omega$")
            if d==1:
              #R1.rotate(90*DEGREES,about_point=[0,0,0])
              R1.rotate_about_origin(90*DEGREES) #.shift(LEFT*0.5)
              #else:
              #R1.shift(UP*0.5)  
            R1.shift(1*UP*(i-imax//2) + 1*RIGHT*(j-jmax//2))
            if not (j==jmax and d==0) and not (i==imax and d==1): 
              self.add(R1)
          #self.wait(0.1)
      mume=MultiMeter(Point([-1.6,-0.8,0]),[1,1,0],[0,0,0],disp="0.63662Ω")
      mume.disptxt.font_size=12
      mume.disptxt.shift(0.1*RIGHT)
      self.play(FadeIn(mume))
      self.add(mume)
      self.play(self.camera.frame.animate.move_to(Point([0.0,0.5,0])).scale(0.4),run_time=5.0)
      
      
      self.wait_till("30:04")

      
      