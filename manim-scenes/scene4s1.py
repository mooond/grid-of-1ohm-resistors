from manim import *
import numpy as np
#from manim.imports import * 
from manim_physics import *
import lcapy

def myscale(obj,fact):
  # scale around origin
  c=obj.get_center()
  obj.scale(fact)
  obj.shift((fact-1)*c)
  return obj

def mkcoordmn(m,n):
  if m < 0:
    coord=f"m-{abs(m)},"
  elif m > 0:
    coord=f"m+{abs(m)},"
  else:
    coord=f"m,"  
  if n < 0:
    coord += f"n-{abs(n)}"
  elif n > 0:
    coord += f"n+{abs(n)}"
  else:
    coord += f"n"  
  coord=coord.replace('-',r'‒') #unicode minus
  return coord

def mkcoord(m,n):
  if m < 0:
    coord=f"-{abs(m)},"
  elif m > 0:
    coord=f"{abs(m)},"
  else:
    coord=f"0,"  
  if n < 0:
    coord += f"-{abs(n)}"
  elif n > 0:
    coord += f"{abs(n)}"
  else:
    coord += f"0"  
  coord=coord.replace('-',r'‒') #unicode minus
  return coord

class ResGraph(Axes):
   def __init__(self,txt=None):
        super().__init__()
        ax = Axes() #.add_coordinates()
        point = ax.c2p(0, 1.5)
        point2 = ax.c2p(0, -1.5)
        dot = Cross(color=YELLOW,stroke_color=YELLOW).scale(0.1).move_to(point)
        dot2 = Cross(color=YELLOW,stroke_color=YELLOW).scale(0.1).move_to(point2)
        #txt=MathTex(r"iK",font_size=24).next_to(point,LEFT)
        # line = axget_horizontal_line(point, line_func=Line)
        graph = ax.plot(lambda x: 3*np.sqrt(1-x*x/9.0), color=BLUE,x_range=(-3,3))
        graph2 = ax.plot(lambda x: 0.05, color=BLUE,x_range=(-3,3))
        a1=Arrow(start=ax.c2p(0.01,3), end=ax.c2p(-0.01,3), color=BLUE,max_tip_length_to_length_ratio=30.0)
        a2=Arrow(start=ax.c2p(-1.01,0), end=ax.c2p(-0.99,0), color=BLUE,max_tip_length_to_length_ratio=30.0)
        self.add(graph,dot,dot2,ax,graph2,a1,a2)
        if txt:
          self.add(txt.next_to(point,LEFT))
        #return self
        

class MultiMeter(VGroup):
   def __init__(self,pos=None,red=None,black=None,rtred=35,rtblack=-35,disp=None):
        super().__init__()
        self.pos=pos
        mm = SVGMobject("multimeter.svg",stroke_width=0.0).scale(1.5)
        rt=  SVGMobject("redtip.svg",stroke_width=0.0)
        bt=  SVGMobject("blacktip.svg",stroke_width=0.0)
        self.mm=mm
        if pos:
          mm.move_to(pos)
        self.add(mm)
        if red:
          rt.move_to(red)
          rt.shift(rt.height*DOWN*0.5)
          rtarget=rt.get_top() + [rt.height*np.sin(rtred*DEGREES),-rt.height*np.cos(rtred*DEGREES),0]
          rt.rotate(rtred*DEGREES,about_point=rt.get_top())
          #t1=red.get_top()
          #delta=rt.get_top()-t1
          #rt.move_to(UP*delta)
          self.add(rt)
          rstart=mm.get_bottom()+RIGHT*0.6+UP*0.57
          bezier = CubicBezier(rstart, rstart + 1 * RIGHT, rtarget + [rt.height*np.sin(rtred*DEGREES),-rt.height*np.cos(rtred*DEGREES),0] , rtarget, stroke_width=6)
          bezier.color="#A00000"
          self.add(bezier)
          
        if black:
          bt.move_to(black)
          bt.shift(bt.height*DOWN*0.5)
          btarget=bt.get_top() + [bt.height*np.sin(rtblack*DEGREES),-bt.height*np.cos(rtblack*DEGREES),0]
          bt.rotate(rtblack*DEGREES,about_point=bt.get_top())
          #t2=black.get_top()
          #delta=bt.get_top()-t2
          #bt.move_to(UP*delta)  
          self.add(bt)
          bstart=mm.get_bottom()+RIGHT*0.6+UP*0.32
          bezier = CubicBezier(bstart, bstart - 1 * UP, btarget + [bt.height*np.sin(rtblack*DEGREES),-bt.height*np.cos(rtblack*DEGREES),0] , btarget, stroke_width=6)
          bezier.color="#000000"
          self.add(bezier)
        if disp:
          disptxt=Text(disp,font="DSEG14 Classic",font_size=20,color=BLACK)
          disptxt.move_to(mm.get_top() + DOWN*0.4+LEFT*0.15)
          self.disptxt=disptxt
          self.add(disptxt)
          
   def update_text(self,newtext):
      disptxt=self.disptxt
      newdisp=Text(newtext,font="DSEG14 Classic",font_size=20,color=BLACK)
      newdisp.move_to(self.mm.get_top() + DOWN*0.4+LEFT*0.15)
      return Transform(disptxt,newdisp)

class Resistor(VGroup):
   def __init__(self,txt=None):
        super().__init__()
        self.txt=txt
        self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        self.body=Rectangle(height=0.5, width=0.15, stroke_width=2)
        self.body.shift(2*UP/4)
        for d in self.dots:
          self.add(d)
        self.add(self.body)
        if self.txt:
          self.tex = Tex(txt, font_size=12)
          self.tex.shift(UP/2+RIGHT/5)
          self.add(self.tex)  
        self.add(Line(self.dots[0],UP/4,stroke_width=2))
        self.add(Line(UP*3/4,self.dots[1],stroke_width=2))

class scene4s1(MovingCameraScene):
    def wait_till(self,n):
      ns=n
      ts=self.timestamp
      if isinstance(n,str):
        fval=[1,60,60]
        nfields=n.split(":")
        n=sum([fval*int(nf) for fval,nf in zip(fval,nfields[::-1])])
        print("waitting till",n,ns)
      tdelta=n-self.renderer.time-ts
      if tdelta < 0:
        print("============= BEHIND SCHEDULE ==============")
      elif tdelta > 0:  
        self.wait(tdelta)
        
    def construct(self):
      myTemplate = TexTemplate()
      myTemplate.add_to_preamble(r"\usepackage{trfsigns}")
      self.timestamp=0
      self.camera.background_color = '#302800'
      # r1 = drawgrid(10)
      # r1.draw(style='european',scale=3,filename="tmp0s1.svg",color='white')
      chap1=Tex(r"{\LARGE Chapter IV} \\ The Integral").move_to([0, 3, 0])
      self.play(Write(chap1))
      self.add(chap1)
      leftside=Point([-6,2.5,0])
      self.wait_till("00:15")
      txR=Tex(r"$ R_{m,n} =  \frac{1}{4 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{1- cos(m \alpha) cos(n \beta) }{ 2 - cos(\alpha) - cos(\beta)} \cdot d \alpha \, d \beta$").next_to(chap1,DOWN).shift(0.5*DOWN).scale(1.4)
      self.play(Write(txR),run_time=3.0)
      self.add(txR)
      self.wait_till("00:48")
      txRn=Tex(r"$ R_{n,n} =  \frac{1}{4 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{1- cos(n \alpha) cos(n \beta) }{ 2 - cos(\alpha) - cos(\beta)} \cdot d \alpha \, d \beta$").scale(1.4).next_to(txR,DOWN)
      self.play(Write(txRn),run_time=3.0)
      self.add(txRn)
      self.wait_till("01:08")

      #
      self.clear()
      tx1=Text("Usefull Trig Identities",font_size=64).move_to([0,3.5,0])
      self.add(tx1)
      txEcos=Tex(r"$cos( \alpha)  =  \frac{e^{j \alpha}  +   e^{- j \alpha}}{2} $" ).scale(1.4).next_to(leftside,DOWN).align_to(leftside,LEFT)
      self.add(txEcos)
      txEsin=Tex(r"$sin( \alpha) =  \frac{e^{j \alpha} -   e^{- j \alpha}}{2j} $" ).scale(1.4).next_to(txEcos,DOWN).align_to(leftside,LEFT)
      self.add(txEsin)
      tx2=Tex(r"$ cos(\alpha ) \cdot cos(\beta ) - sin(\alpha)\cdot\sin(\beta) = \cos(\alpha + \beta ) $ ").next_to(txEsin,DOWN).align_to(leftside,LEFT)
      self.add(tx2)
      self.wait_till("01:59")
      #
      self.clear()
      tx1=txRn.shift(3*UP)
      tx1[0][20:34].color=RED
      self.add(tx1)
      self.wait_till("02:10")
      tx2=tx2.next_to(tx1,DOWN).align_to(leftside,LEFT)
      self.add(tx2)
      self.wait_till("02:20")
      txRn2=Tex(r"$ R_{n,n} =  \frac{1}{4 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{1- cos(n ( \alpha + \beta)) }{ 2 - cos(\alpha) - cos(\beta)} \cdot d \alpha \, d \beta$").scale(1.4).next_to(tx2,DOWN).align_to(tx1,LEFT)
      txRn2[0][20:31].color=BLUE
      self.play(Write(txRn2),run_time=3.0)
      self.add(txRn2)
      self.wait_till("02:45")
      tx3=Tex(r"$ f_{Odd}(x) = - f_{Odd}(-x) $ \hspace{1cm} $ f_{Even}(x) = f_{Even}(-x)  $").next_to(txRn2,DOWN).align_to(tx1,LEFT)
      self.play(Write(tx3),run_time=3.0)
      self.add(tx3)
      self.wait_till("03:13")
      tx4=Tex(r"$ \int\limits_{-\pi}^{\pi} f_{Odd}(x) d x = 0  $ \hspace{1cm} $ f_{Odd}(x) \cdot f_{Even}(x) = f_{Odd_2}(x)  $").next_to(tx3,DOWN).align_to(tx1,LEFT)
      self.play(Write(tx4),run_time=3.0)
      self.add(tx4)
      self.wait_till("04:06")
      self.clear()
      tx1=Tex(r"$ R_{n,n} =  \frac{1}{4 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{1- cos(n ( \alpha + \beta)) }{ 2 - cos(\alpha) - cos(\beta)} \cdot d \alpha \, d \beta$").scale(1.4).move_to([0,3,0]).align_to(leftside,LEFT)
      self.add(tx1)
      txu=Tex(r"$ u + v = \alpha $").next_to(tx1,DOWN).align_to(leftside,LEFT)
      txv=Tex(r"$ u - v = \beta  $").next_to(txu,DOWN).align_to(leftside,LEFT)
      self.add(txu,txv)
      self.wait(5)
      txdudv=Tex(r"$ d \alpha \, d \beta = \frac{d u \, d v}{2} $").next_to(txv,DOWN).align_to(leftside,LEFT)
      self.add(txdudv)
      self.wait_till("04:21")
      
      diag=VGroup()
      axes = Axes(x_range = (-7, 7), y_range=(-7, 7), x_length=4, y_length=4,  axis_config={"include_tip": False})
      diag.add(axes)
      func = axes.plot(lambda x: x, color=BLUE)
      func2 = axes.plot(lambda x: -x, color=BLUE)
        # creates the T_label
        
      #px_label = axes.get_T_label(x_val=np.pi, label=Tex(r"$\pi$"))
      #mx_label = axes.get_T_label(x_val=-np.pi,graph=func, label=Tex("r$- \pi$"))
      xylab = axes.get_axis_labels(
            Tex(r"$\alpha$"), Tex(r"$\beta$")
      )
      r1=Rectangle(width=axes.x_axis.n2p(np.pi)[0]-axes.x_axis.n2p(-np.pi)[0],
                   height=axes.y_axis.n2p(np.pi)[1]-axes.y_axis.n2p(-np.pi)[1])
      r2=Rectangle(width=r1.width*np.sqrt(2),
                   height=r1.height*np.sqrt(2),color=YELLOW).rotate(45*DEGREES)
      diag.add(axes, func,func2,r1,r2,xylab)
      self.add(diag.shift(DOWN+4.5*RIGHT))
      x_tex_lables = VGroup(*[
        MathTex(r"-2 \pi").next_to(axes.x_axis.n2p(-2*np.pi),DOWN+LEFT),
        MathTex(r"2 \pi").next_to(axes.x_axis.n2p(+2*np.pi),DOWN+RIGHT),
      ])  
      y_tex_lables = VGroup(*[
        MathTex(r"-2 \pi").next_to(axes.y_axis.n2p(-2*np.pi),DOWN+RIGHT),
        MathTex(r"2 \pi").next_to(axes.y_axis.n2p(+2*np.pi),UP+LEFT),
      ])  
      self.add(x_tex_lables,y_tex_lables)
      self.wait_till("04:36")
      tx2=Tex(r"$ R_{n,n} =  \frac{1}{4 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{1- cos(2 n u ) }{ 2 - cos(u + v ) - cos(u - v )} \cdot d u \, d v$").scale(0.9).next_to(txdudv,DOWN).align_to(leftside,LEFT)
      self.add(tx2)
      self.wait_till("06:01")
      #
      #
      self.clear()
      tx2=Tex(r"$ R_{n,n} =  \frac{1}{4 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{1- cos(2 n u ) }{ 2 - cos(u + v ) - cos(u - v )} \cdot d u \, d v$").scale(1.2).move_to([0,3,0]).align_to(leftside,LEFT)
      self.add(tx2)
      tx3=Tex(r"$ cos(u+v) + cos(u-v) = 2 \cdot cos(u) \cdot cos(v) $").next_to(tx2,DOWN).align_to(leftside,LEFT)
      self.wait_till("06:08")
      self.add(tx3)
      tx4=Tex(r"$ R_{n,n} =  \frac{1}{4 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{1- cos(2 n \alpha ) }{ 2 - 2 \cdot cos(\alpha )\cdot cos(\beta )} \cdot d \alpha \, d \beta$").scale(1.2).next_to(tx3,DOWN).align_to(leftside,LEFT)
      self.wait_till("06:14")
      self.add(tx4)
      tx5=Tex(r"$ R_{n,n} =  \frac{1}{8 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{1- cos(2 n \alpha ) }{ 1 - cos(\alpha )\cdot cos(\beta )} \cdot d \alpha \, d \beta$").scale(1.2).next_to(tx4,DOWN).align_to(leftside,LEFT)
      self.wait_till("06:18")
      self.add(tx5)
      tx6=Tex(r"$  B = \int\limits_{-\pi}^{\pi} \frac{1}{ 1 - K \cdot cos(\beta )} \cdot d \beta$").scale(1.2).next_to(tx5,DOWN).align_to(leftside,LEFT)
      self.wait_till("06:41")
      self.add(tx6)
      self.wait_till("07:20")
      #
      #
      self.clear()
      tx1=Tex(r"$  B = \int\limits_{-\pi}^{\pi} \frac{1}{ 1 - K \cdot cos(\beta )} \cdot d \beta$").move_to([0,3,0]).align_to(leftside,LEFT)
      self.add(tx1)
      self.wait(10)
      tx2=Tex(r"$ t=tan( \frac{\beta}{2}) $ \\ $ cos(\beta)= \frac{1-t^2}{1+t^2} $ \\ $ d \beta = \frac{2}{1+t^2} d t $").next_to(tx1,DOWN).align_to(leftside,LEFT)
      self.add(tx2)
      self.wait_till("08:00") 
      tx3=Tex(r"$ B = \int\limits_{-\infty}^{+\infty} \frac{2}{t^2 \cdot (K+1) + 1-K }  d t  $").next_to(tx2,DOWN).align_to(leftside,LEFT) 
      self.add(tx3)
      self.wait_till("08:24")
      tx3b=Tex(r"$ K < 1 $").move_to([5,-1,0])
      self.add(tx3b)
      self.wait(5)
      tx3c=Tex(r"$ t_{1,2}= \pm j \cdot \sqrt{\frac{1-K}{1+K} } $").next_to(tx3b,DOWN).align_to(tx3b,RIGHT)
      self.add(tx3c)
      self.wait_till("08:42")
      #
      #
      txx=Tex(r"$ j \cdot \sqrt{\frac{1-K}{1+K} } $",font_size=36)
      resax=ResGraph(txx).scale(0.6).shift(3*RIGHT+2*UP)
      self.add(resax)
      
      self.wait_till("09:50")
      tx4=Tex(r"$ B= 2 \pi \cdot j \cdot \mathop{\mathrm{Res}}\limits_{t=t_1}(\frac{2}{t^2 \cdot (K+1) + 1-K })  $ ").next_to(tx3,DOWN).align_to(leftside,LEFT)
      self.add(tx4)
      self.wait_till("10:03")
      tx5=Tex(r"$ B= 2 \pi \cdot j \cdot \lim_{t \to t_1} \frac{2}{(K+1) \cdot (t+t_1)} = \frac{2 \pi}{\sqrt{1-K^2}} $").next_to(tx4,DOWN).align_to(leftside,LEFT)
      self.add(tx5)
      self.wait_till("10:30")
      #
      #
      self.clear()
      tx1=Tex(r"$  B = \int\limits_{-\pi}^{\pi} \frac{1}{ 1 - K \cdot cos(\beta )} \cdot d \beta $ \\ $ K=cos(\alpha) $").move_to([0,3,0]).align_to(leftside,LEFT)
      self.add(tx1)
      self.wait_till("10:42")
      tx2=Tex(r"$ B = \frac{2 \pi}{\sqrt{1-K^2}} $").next_to(tx1,DOWN).align_to(leftside,LEFT)
      self.add(tx2)
      tx3=Tex(r"$ B = \frac{2 \pi}{sin \alpha} $ \hspace{1cm} $ 0 < \alpha <  \pi $  ").next_to(tx2,DOWN).align_to(leftside,LEFT)
      self.add(tx3)
      self.wait_till("11:04")
      #
      self.clear()
      tx1=Tex(r"$ R_{n,n} =  \frac{1}{8 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{1- cos(2 n \alpha ) }{ 1 - cos(\alpha )\cdot cos(\beta )} \cdot d \alpha \, d \beta$  \\ $ 1-cos(2x) = 2\cdot sin^2(x) $ ").move_to([0,3,0]).align_to(leftside,LEFT)
      self.add(tx1)
      self.wait_till("11:27")
      tx2=Tex(r"$ R_{n,n} =  \frac{1}{4 \pi^2} \int\limits_{-\pi}^{\pi}\int\limits_{-\pi}^{\pi} \frac{sin^2( n \alpha ) }{ 1 - cos(\alpha )\cdot cos(\beta )} \cdot d \alpha \, d \beta$").next_to(tx1,DOWN).align_to(leftside,LEFT)
      self.add(tx2)
      self.wait_till("11:40")
      tx3=Tex(r"$ R_{n,n} =  \frac{1}{2 \pi^2} \int\limits_{0}^{\pi} \bigl( sin^2( n \alpha ) \cdot  \int\limits_{-\pi}^{\pi} \frac{1}{ 1 - cos(\alpha )\cdot cos(\beta )} \cdot d \beta \bigr) \, d \alpha$").next_to(tx2,DOWN).align_to(leftside,LEFT)
      self.add(tx3)
      self.wait(5)
      tx5=Tex(r"$  B = \int\limits_{-\pi}^{\pi} \frac{1}{ 1 - cos(\alpha) \cdot cos(\beta )} \cdot d \beta = \frac{2 \pi}{sin \alpha} $").next_to(tx3,DOWN).align_to(leftside,LEFT)
      self.add(tx5)
      self.wait_till("12:35")
      tx6=Tex(r"$  R_{n,n} =  \frac{1}{\pi} \int\limits_{0}^{\pi} \frac{sin^2( n \alpha )}{sin(\alpha)} d \alpha  $").next_to(tx5,DOWN).align_to(leftside,LEFT)
      self.add(tx6)
      self.wait_till("12:55")
      #
      #
      self.clear()
      tx1=Tex(r"$  R_{n,n} =  \frac{1}{\pi} \int\limits_{0}^{\pi} \frac{sin^2( n \alpha )}{sin(\alpha)} d \alpha  $").scale(1.4).move_to([0,3,0]).align_to(leftside,LEFT)
      bg1=BackgroundRectangle(tx1, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
      self.add(bg1,tx1)
      self.wait_till("13:06")
      tx2=Tex(r"$  R_{1,1} =  \frac{1}{\pi} \int\limits_{0}^{\pi} sin( \alpha ) d \alpha  = \frac{1}{\pi} ( -cos(\alpha)) \biggr\rvert_0^{\pi} = \frac{1}{\pi} (-(-1) + 1) = \frac{2}{\pi}  $").next_to(tx1,DOWN).align_to(leftside,LEFT)
      self.add(tx2)
      self.wait_till("13:46")
      tx3=Tex(r"$ sin^2(n \cdot \alpha)= sin(\alpha) \cdot  \sum\limits_{k=1}^n sin((2k-1)\alpha) $").next_to(tx2,DOWN).align_to(leftside,LEFT)
      self.add(tx3)
      self.wait_till("14:07")
      tx4=Tex(r"$ R_{n,n} =  \frac{1}{\pi} \sum\limits_{k=1}^n \frac{-1}{2k-1} \cdot cos((2k-1)\alpha) \biggr\rvert_0^{\pi} $").next_to(tx3,DOWN).align_to(leftside,LEFT)
      self.add(tx4)
      self.wait_till("14:21")
      tx5=Tex(r"$ R_{n,n} =  \frac{2}{\pi} \sum\limits_{k=1}^n \frac{1}{2k-1} = \frac{2}{\pi} \cdot (\frac{1}{1}  + \frac{1}{3} + \frac{1}{5} + \ldots \frac{1}{2n-1} )   $").next_to(tx4,DOWN).align_to(leftside,LEFT)
      self.add(tx5)
      self.wait_till("14:55")
      self.clear()
      tx1=Tex(r"$ R_{n,n} =  \frac{2}{\pi} \sum\limits_{k=1}^n \frac{1}{2k-1} = \frac{2}{\pi} \cdot (\frac{1}{1}  + \frac{1}{3} + \frac{1}{5} + \ldots \frac{1}{2n-1} )   $").scale(1.2).move_to([0,0,0]).align_to(leftside,LEFT)
      bg1=BackgroundRectangle(tx1, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
      self.add(bg1)
      self.add(tx1)  
      self.wait_till("15:08")
      self.clear()
      numplane=NumberPlane(x_range=(-2, 9, 1), y_range=(-2, 4, 1))
      self.add(numplane)
      rdots=[]
      xdots=[]
      for m in range(-2,4,1):
        for n in range(-2,9,1):#
          coord=mkcoord(m,n)
          color=YELLOW if abs(m) == abs(n) or (abs(m) < 2 and abs(n) < 2) else WHITE
          dt=Dot(numplane.coords_to_point(n,m),color=color,radius=0.07)
          if m==2 and (n >3 and n < 7):
            rdots.append(dt)
          if n==5 and (m==1 or m==3):
            rdots.append(dt)  
          if (m==2 or n==2) and abs(m-n)==1 :
            xdots.append(dt)
          self.add(Tex(r"$ F_{" + coord + "} $",font_size=20).align_to([0.1,0.1,0]+numplane.coords_to_point(n,m),LEFT+DOWN))
          self.add(dt)
      #self.add(q1)
      self.wait_till("15:34")
      r1=Rectangle(width=2.5,height=0.5).move_to([1.5,1,0])
      r2=Rectangle(width=2.5,height=0.5).move_to([1.5,1,0]).rotate(90*DEGREES)
      self.add(r1,r2)
      for dt in rdots:
        dt.color=RED
      rdots[3].color=BLUE  
      self.wait_till("16:29")
      r1.shift(3*LEFT)
      r2.shift(3*LEFT)
      for dt in rdots:
        dt.color=WHITE
      for dt in xdots:
        dt.color=RED
      xdots[3].color=BLUE
      xdots[2].color=BLUE  
      self.wait_till("17:20")
      self.clear()
      rrimg=ImageMobject('rrpy1.png').scale(2.0)
      self.play(FadeIn(rrimg))
      self.add(rrimg)
      self.wait(5)
      self.play(FadeOut(rrimg))
      self.remove(rrimg)
      self.wait_till("20:04")
      rrimg=ImageMobject('rrpy2.png').scale(1.6)
      self.play(FadeIn(rrimg))
      self.add(rrimg)
      self.wait(5)
      self.play(FadeOut(rrimg))
      self.remove(rrimg)
      self.wait_till("21:43")
      rrimg=ImageMobject('rrpy3.png').scale(1.6)
      self.play(FadeIn(rrimg))
      self.add(rrimg)
      self.wait(5)
      self.play(FadeOut(rrimg))
      self.remove(rrimg)
      self.wait_till("21:59")
      rrimg=ImageMobject('rrpy4.png').scale(1.8)
      self.play(FadeIn(rrimg))
      self.add(rrimg)
      self.wait_till("22:20")
      self.clear()
      imax=11
      jmax=13
      for d in range(2):
        for i in range(imax+1):
          for j in range(jmax+1):
            R1=Resistor(r"$1\Omega$")
            if d==1:
              #R1.rotate(90*DEGREES,about_point=[0,0,0])
              R1.rotate_about_origin(90*DEGREES) #.shift(LEFT*0.5)
              #else:
              #R1.shift(UP*0.5)  
            R1.shift(1*UP*(i-imax//2) + 1*RIGHT*(j-jmax//2))
            if not (j==jmax and d==0) and not (i==imax and d==1): 
              self.add(R1)
          #self.wait(0.1)
      mume=MultiMeter(Point([-1.6,-0.8,0]),[1,1,0],[0,0,0],disp=".")
      tx1=Tex(r"$ \frac{2}{\pi}\Omega $",font_size=28,color=BLACK)
      self.play(FadeIn(mume))
      self.add(mume)
      self.remove(mume.disptxt)
      self.add(tx1.next_to(mume.disptxt,LEFT).shift(0.8*RIGHT))
      self.play(self.camera.frame.animate.move_to(Point([0.0,0.5,0])).scale(0.4),run_time=5.0)

      self.wait_till("22:35")

