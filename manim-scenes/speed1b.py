import numpy as np
N=10_000_000
x=np.random.rand(N)
y=np.random.rand(N)
r2=x*x + y*y 
isin=np.where(r2 <= 1.0 , 1,0)
print("pi is about",4.0*np.sum(isin)/N)
