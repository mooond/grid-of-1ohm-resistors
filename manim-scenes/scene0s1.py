from manim import *
#from manim.imports import * 
from manim_physics import *
#from lcapy import *


class Resistor(VGroup):
   def __init__(self,txt=None):
        super().__init__()
        self.txt=txt
        self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        self.body=Rectangle(height=0.5, width=0.15, stroke_width=2)
        self.body.shift(2*UP/4)
        for d in self.dots:
          self.add(d)
        self.add(self.body)
        if self.txt:
          self.tex = Tex(txt, font_size=12)
          self.tex.shift(UP/2+RIGHT/5)
          self.add(self.tex)  
        self.add(Line(self.dots[0],UP/4,stroke_width=2))
        self.add(Line(UP*3/4,self.dots[1],stroke_width=2))


class scene0s1(MovingCameraScene):
    def wait_till(self,n):
      ns=n
      ts=self.timestamp
      if isinstance(n,str):
        fval=[1,60,60]
        nfields=n.split(":")
        n=sum([fval*int(nf) for fval,nf in zip(fval,nfields[::-1])])
        print("waitting till",n,ns)
      tdelta=n-self.renderer.time-ts
      if tdelta < 0:
        print("============= BEHIND SCHEDULE ==============")
      elif tdelta > 0:  
        self.wait(tdelta)
        
    def construct(self):
      self.timestamp=0
      self.camera.background_color = '#302800'
      # r1 = drawgrid(10)
      # r1.draw(style='european',scale=3,filename="tmp0s1.svg",color='white')
      for d in range(2):
        for i in range(20):
          for j in range(20):
            R1=Resistor(r"$1 \Omega$")
            if d==1:
              #R1.rotate(90*DEGREES,about_point=[0,0,0])
              R1.rotate_about_origin(90*DEGREES)
            R1.shift(UP*(i-10) + RIGHT*(j-10))
            self.add(R1)
          #self.wait(0.1)
      self.wait_till("00:09")
      self.camera.frame.save_state()
      self.play(self.camera.frame.animate.move_to(2.1*UP/5+2*LEFT/5))
      # 00:10
      self.add(Dot(color=RED,radius=0.1))
      self.add(Dot(UP+LEFT,color=RED,radius=0.1))
      self.play(self.camera.frame.animate.set(width=2.1),run_time=14.0)
      self.wait(1)
      # 00:25
      gtxt=Tex(r"An Infinite Grid of $ 1\Omega$ Resistors",font_size=5).shift(0.85*UP+LEFT/2)
      gtxt.z_index=5
      self.play(Write(gtxt),run_time=3.0)
      self.wait(1)
      # 00:28
      a1=Arrow(start=0.7*(UP+LEFT), end=Point([-1,1,0]), color=YELLOW)
      a1.z_index=2
      self.add(a1)
      a2=Arrow(start=0.3*(UP+LEFT), end=Point([0,0,0]), color=YELLOW)
      a2.z_index=2
      self.add(a2)
      dist=Tex(r"What is the resistnace \\ between these 2 Points?",font_size=5).rotate(-45*DEGREES).shift(5*UP/10+LEFT/2)
      dist2=Tex(r"$ \frac{2}{\pi} $",font_size=18).rotate(-45*DEGREES).shift(5*UP/10+LEFT/2)
      dist3=Tex(r"$ \frac{1}{2} $",font_size=18).shift(5*UP/10+LEFT/2)
      dist.z_index=2
      self.play(Write(dist),run_time=2.0)
      # 00:30
      self.wait(3)
      # 00:33
      self.play(ReplacementTransform(dist, dist2),run_time=3)
      #self.add(Dot(color=BLUE,radius=0.1))
      #self.add(Dot(UP+LEFT,color=BLUE,radius=0.1))
      self.wait_till(40)
      self.add(Dot(UP,color=RED,radius=0.1))
      a3=Arrow(start=0.7*UP+0.3*LEFT, end=Point([0,1,0]), color=YELLOW)
      #self.play(Restore(self.camera.frame),run_time=5.0)
      self.play(FadeOut(a1),FadeOut(dist),FadeOut(dist2))
      self.play(FadeIn(a3),FadeIn(dist3))
      self.wait(5)
      self.play(Restore(self.camera.frame),run_time=30.0)
      self.wait_till("01:50")
      self.clear()
      chap1=Tex(r"{\LARGE Chapter I} \\ Electrical Resistance and Simple Electrical Networks").move_to([0, 3, 0])
      self.play(Write(chap1))
      self.add(chap1)
      self.wait(1)
      b1=[ (r"Basics of Electricity: Voltage, Potential, Current", "02:12"),
           (r"Resistors: Ohm's Law", "02:22"),
           (r"Connecting Multiple Reistors together", "02:27"),
           (r"Kirchhoff's Law", "02:30"),
           (r"The Superposition Principle", "02:34"),
           (r"An easier Infinite Network of Resitors", "02:36"),
           (r"The Infinite Grid - Our First Victory Neighboring Points", "02:40"),
          ]
      toppos=chap1.get_bottom()     
      for btext,btime in b1:
        print("adding,",btext,"at",btime)
        self.wait_till(btime)
        btex=Tex(btext).move_to(toppos+DOWN*0.6)
        self.play(Write(btex))
        self.add(btex)
        self.play(Indicate(btex),runt_time=2.0)
        #self.play(Indicate(btex),2)
        toppos=btex.get_bottom()
      self.wait(5)  
      self.clear()
      chap2=Tex(r"{\LARGE Chapter II} \\ Taming Infinity with Generating Functions, Z-Transform").move_to([0, 3, 0])
      self.play(Write(chap2))
      self.add(chap2)
      self.wait_till("03:00")
      b1=[ (r"The equations for our Infinit Grid of Resistors", "03:04"),
           (r"The discrete laplacian", "03:06"),
           (r"Powerful Tools: Generating Functions and Z-Transform", "03:09"),
           (r"How to solve 1 dimensional difference equations", "03:12"),
           (r"Example: Fibonacci Numbers", "03:15"),
           (r"Trigonometric Functions as Solutions", "03:18"),
           (r"The Characteristic Polynomial", "03:20"),
          ]
      toppos=chap2.get_bottom()     
      for btext,btime in b1:
        print("adding,",btext,"at",btime)
        self.wait_till(btime)
        btex=Tex(btext).move_to(toppos+DOWN*0.6)
        self.play(Write(btex))
        self.add(btex)
        self.play(Indicate(btex),runt_time=2.0)
        #self.play(Indicate(btex),2)
        toppos=btex.get_bottom()
      self.wait_till("03:26")  
      self.clear()
      #
      chap3=Tex(r"{\LARGE Chapter III} \\ Finding a Solution for the Infinite Grid of Resistors \\ with Fourier Series").move_to([0, 3, 0])
      self.play(Write(chap3))
      self.add(chap3)
      self.wait_till("03:28")
      b1=[ (r"Solutions of differential equations instead\\ of difference equations. Why is it easier?", "03:35"),
           (r"What is a Green's Function and why do we want one?", "03:43"),
           (r"Constructing the Green's Function for our Equation", "03:48"),
           (r"Calculating the Resistance from the Potential", "03:52")
          ]
      toppos=chap3.get_bottom()     
      for btext,btime in b1:
        print("adding,",btext,"at",btime)
        self.wait_till(btime)
        btex=Tex(btext).move_to(toppos+DOWN*0.6)
        self.play(Write(btex))
        self.add(btex)
        self.play(Indicate(btex),runt_time=2.0)
        #self.play(Indicate(btex),2)
        toppos=btex.get_bottom()
      self.wait_till("03:54")  
      self.clear()
      #
      chap4=Tex(r"{\LARGE Chapter IV} \\ Solving the Integral").move_to([0, 3, 0])
      self.play(Write(chap4))
      self.add(chap4)
      self.wait_till("03:55")
      b1=[ (r"Solving the Integral", "04:00"),
           (r"Calculating a Resistance from its Neighbors", "04:05")
          ]
      toppos=chap4.get_bottom()     
      for btext,btime in b1:
        print("adding,",btext,"at",btime)
        self.wait_till(btime)
        btex=Tex(btext).move_to(toppos+DOWN*0.6)
        self.play(Write(btex))
        self.add(btex)
        self.play(Indicate(btex),runt_time=2.0)
        #self.play(Indicate(btex),2)
        toppos=btex.get_bottom()
      self.wait_till("04:20")  
      self.clear()
      oblue=ImageMobject('outoftheblue.png')
      oblue.shift(-0.5 * UP + 0*RIGHT)
      oblue.scale(2)
      self.play(FadeIn(oblue))
      self.add(oblue)
      self.camera.frame.save_state()
      self.play(self.camera.frame.animate.move_to(Point([-2.0,-1.0,0])).scale(0.38),run_time=8.0)
      self.wait_till("04:35")
      self.play(Restore(self.camera.frame),run_time=5.0)
      self.play(FadeOut(oblue))
      self.clear()
      R1=Resistor(r"$1 \Omega$").scale(3)
      self.play(FadeIn(R1))
      self.wait(5)

      
      
      
      