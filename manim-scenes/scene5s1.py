from manim import *
import numpy as np
#from manim.imports import * 
from manim_physics import *
import lcapy, os
from textwrap import dedent

def fourshift(rin):
   rout=np.zeros(np.shape(rin))
   rout[1:,:] += rin[:-1,:]
   rout[:-1,:] += rin[1:,:]
   rout[:,1:] += rin[:,:-1]
   rout[:,:-1] += rin[:,1:]
   return rout      
N=201
H=N //2
I=np.zeros((N,N))
I[H,H]=-2
ir=2.0/4/(N-1)
I[0,:]=ir
I[-1,:]=ir
I[:,0]=ir
I[:,-1]=ir
z=np.ones((N,N))
zz=fourshift(z)
rz=1/zz
def L(rin):
    #print(np.shape(rin))
    ro=fourshift(rin)
    ro = (ro+I)  *rz
    ro -= ro[H,H]
    return ro

class ErrLogGraph(VGroup):
   def __init__(self):
        super().__init__()
        err=[]
        global N
        if os.path.isfile("errlog.npz"):
          with np.load("errlog.npz") as data:
            errlog=data['errlog']
          print("errlog shape=",errlog[0:5])
        else:  
          print("recalculating errlog")
          r1=np.zeros((N,N))
          for i in range(150000):
            rn=L(r1)
            err1=(rn-r1)*(rn-r1)
            errsum=np.sqrt(np.sum(err1,axis=(0,1))/N/N)
            err.append(errsum)
            r1 += (rn-r1)*0.95
          errlog=np.log(np.array(err))/np.log(10)
          with open('errlog.npz', 'wb') as fh:
            np.savez(fh, errlog=errlog)
        ax = Axes(
                y_range=[-16, 0, 1],
                x_range=[0, 140000, 25000],
                x_length=12,
                y_length=5,
                tips=False,
                axis_config={"include_numbers": True},
                #x_axis_config={"scaling": LogBase(custom_labels=True)},
            )

            # x_min must be > 0 because log is undefined at 0.
        graph = ax.plot(lambda x: errlog[ int(x) ] , x_range=[0, 140000], color=BLUE)
        #graph = ax.plot(lambda x: np.log(0.01/(1+0.01*x))/np.log(10),use_smoothing=False)
        self.add(ax, graph)    
        #self.add(Dot([0,0,0],radius=0.1))
    
    
def myscale(obj,fact):
  # scale around origin
  c=obj.get_center()
  obj.scale(fact)
  obj.shift((fact-1)*c)
  return obj

class scene5s1(MovingCameraScene):
    def wait_till(self,n):
      ns=n
      ts=self.timestamp
      if isinstance(n,str):
        fval=[1,60,60]
        nfields=n.split(":")
        n=sum([fval*int(nf) for fval,nf in zip(fval,nfields[::-1])])
        print("waitting till",n,ns)
      tdelta=n-self.renderer.time-ts
      if tdelta < 0:
        print("============= BEHIND SCHEDULE ==============")
      elif tdelta > 0:  
        self.wait(tdelta)
        
    def construct(self):
      myTemplate = TexTemplate()
      myTemplate.add_to_preamble(r"\usepackage{trfsigns}")
      self.timestamp=0
      self.camera.background_color = '#302800'
      # r1 = drawgrid(10)
      # r1.draw(style='european',scale=3,filename="tmp0s1.svg",color='white')
      chap1=Tex(r"{\LARGE Chapter V} \\ Finite Grid \\ Numeric Simulations with Numpy").to_edge(UP,buff=0.3)
      self.play(Write(chap1))
      self.add(chap1)
      leftside=Point().to_edge(UL,buff=0.5)
      self.wait_till("00:05")
      txt1=Text("• Python, Numpy, Scipy",font_size=36).next_to(chap1,DOWN,buff=0.8).to_edge(LEFT,buff=0.5)
      self.play(Write(txt1),run_time=3.0)
      self.add(txt1)
      self.wait_till("01:40")
      txt2=Text("• Numpy Basics, Slicing",font_size=36).next_to(txt1,DOWN,buff=0.4).to_edge(LEFT,buff=0.5)
      self.play(Write(txt2),run_time=3.0)
      self.add(txt2)
      self.wait_till("02:14")
      txt3=Text("• Relaxation Method",font_size=36).next_to(txt2,DOWN,buff=0.4).to_edge(LEFT,buff=0.5)
      self.play(Write(txt3),run_time=3.0)
      self.add(txt3)
      self.wait_till("02:42")
      txt4=Text("• Solving With Sparse Matrices",font_size=36).next_to(txt3,DOWN,buff=0.4).to_edge(LEFT,buff=0.5)
      self.play(Write(txt4),run_time=3.0)
      self.add(txt4)
      self.wait_till("02:58")
      #
      #
      self.clear()
      head1=Tex(r"Python, Numpy and Scipy").to_edge(UP,buff=0.3)
      self.play(Write(head1,run_time=3.0))
      self.add(head1)
      self.wait(2)
      txt1=Text("🐌 Python is slow why use it for numeric simulations?",font_size=36).next_to(head1,DOWN,buff=0.8).to_edge(LEFT,buff=0.5)
      self.play(Write(txt1),run_time=3.0)
      self.add(txt1)
      self.wait_till("03:38")
      txt2=Text("Interactive Data exploration with scripting languages: \n R, Octave, MATLAB (comercial$)",font_size=36).next_to(txt1,DOWN,buff=0.8).to_edge(LEFT,buff=0.5)
      self.play(Write(txt2),run_time=3.0)
      self.add(txt2)
      self.wait_till("04:19")
      txt3=Text("Speedup: A factor of 50 is not unusual",font_size=36).next_to(txt2,DOWN,buff=0.8).to_edge(LEFT,buff=0.5)
      self.play(Write(txt3))
      self.add(txt3)
      self.wait_till("05:00")
      #
      self.clear()
      head1=Tex(r"Monte Carlo $\pi$ ").to_edge(UP,buff=0.3)
      self.play(Write(head1))
      self.add(head1)
      self.wait(5)

      axes = Axes(
            x_range=[0, 1, 0.1],
            y_range=[0, 1, 0.1],
            x_length=5,
            y_length=5,
            axis_config={"color": GREEN},
            tips=False,
        )
      circ = axes.plot(lambda x: np.sqrt(1-x*x), color=BLUE)  
      plot=VGroup(axes,circ)
      ps=[axes.c2p(x,y) for x,y in [(0,0),(0,1),(1,1),(1,0)] ]
      lines=[Line(ps[i],ps[(i+1)%4],color=YELLOW) for i in range(len(ps))] 
      print(lines)
      ra=VGroup(*lines)
      self.add(plot,ra) 
      pnts=[]
      N=1000
      np.random.seed(seed=4711)
      for i in range(N):
        x=np.random.rand()
        y=np.random.rand()
        p=Dot(axes.c2p(x,y),radius=0.05,color=RED if x*x + y*y < 1.0 else BLUE)
        pnts.append(p)
      dt=5.0  
      self.wait_till("05:57")
      for i in range(15):
         self.add(pnts[i])
         self.wait(dt)
         dt = dt/1.3
      self.add(*pnts)
      self.add(Tex(r"$ A = \frac{\pi}{4} $",font_size=80).next_to(axes,RIGHT,buff=0.5))  
      self.wait_till("06:28")
      listing1 = Code("speed1a.py",font_size=32,line_spacing=1)
      self.play(Write(listing1),time=5.0)
      self.add(listing1)
      self.wait_till("07:52")
      self.remove(listing1)
      listing2 = Code("speed1b.py",font_size=32,line_spacing=1)
      self.play(Write(listing2),time=5.0)
      self.add(listing2)
      self.wait_till("13:18")
      #
      #
      self.clear()
      head1=Tex(r"Slicing").to_edge(UP,buff=0.3)
      self.play(Write(head1,run_time=3.0))
      self.add(head1)
      self.wait_till("13:45")
      listing1 = Code("slice1a.py",font_size=32,line_spacing=1)
      self.play(Write(listing1),time=5.0)
      self.add(listing1)
      self.wait_till("17:11")
      self.remove(listing1)
      listing2 = Code("slice1b.py",font_size=32,line_spacing=1,line_no_from=10)
      self.play(Write(listing2),time=5.0)
      self.add(listing2)
      self.wait_till("19:50")
      #
      #
      self.clear()
      head1=Tex(r"The Relaxation Method").to_edge(UP,buff=0.3)
      self.play(Write(head1,run_time=3.0))
      self.add(head1)
      tx1=Tex(r"$ \mathbf{L} F_{m,n} =  -4 \cdot F_{m,n}+F_{m+1,n}+F_{m-1,n}+F_{m,n+1}+F_{m,n-1} = -I_{m,n} $",font_size=42).next_to(head1,DOWN).to_edge(LEFT,buff=0.3)
      self.play(Write(tx1),run_time=3.0)
      self.add(tx1)
      self.wait_till("20:35")
      listing1 = Code(code=dedent("""
      N=201
      H=N // 2 
      I=np.zeros((N,N))
      I[H,H]=2
      """),font_size=32,line_spacing=1,language="Python")
      self.add(listing1)
      self.wait_till("22:49")
      self.remove(listing1)
      listing1 = Code(code=dedent("""
      ir=-2.0/4/(N-1)
      I[0,:]=ir
      I[-1,:]=ir
      I[:,0]=ir
      I[:,-1]=ir
      """),font_size=32,line_spacing=1,language="Python",line_no_from=5)
      self.add(listing1)
      self.wait_till("24:14")
      self.remove(listing1)
      listing1 = Code(code=dedent("""
      def fourshift(rin):
        rout=np.zeros(np.shape(rin))
        rout[1:,:] += rin[:-1,:]
        rout[:-1,:] += rin[1:,:]
        rout[:,1:] += rin[:,:-1]
        rout[:,:-1] += rin[:,1:]
        return rout      
      """),font_size=32,line_spacing=1,language="Python",line_no_from=20)
      self.add(listing1)
      self.wait_till("25:42")
      self.remove(listing1)
      #
      #
      #
      z=np.ones((7,7))
      zz=fourshift(z)
      #npstr=np.vectorize(lambda x: str(int(x)))
      t2 = IntegerTable(zz).scale(0.6)
      self.add(t2)
      self.wait_till("27:13")
      listing1 = Code(code=dedent("""
      z=np.ones((N,N))
      zz=fourshift(z)
      rz=1/zz      
      """),font_size=32,line_spacing=1,language="Python",line_no_from=30)
      self.add(listing1)
      self.wait_till("28:01")
      self.remove(listing1,t2)

      listing1 = Code(code=dedent("""
      def Lstep(rin):
        ro=fourshift(rin)
        ro = (ro+I)  *rz
        ro -= ro[H,H]
      return ro
      """),font_size=32,line_spacing=1,language="Python",line_no_from=40)
      self.add(listing1)
      self.wait_till("29:00")
      self.remove(listing1)

      listing1 = Code(code=dedent("""
      r1=np.zeros((N,N))
      for i in range(50000):
        rn=Lstep(r1)
        r1 += (rn-r1)*0.95
      """),font_size=32,line_spacing=1,language="Python",line_no_from=50)
      self.add(listing1)
      self.wait_till("30:00")
      self.remove(listing1)
      #
      #
      self.clear()
      head1=Tex(r"convergence").to_edge(UP,buff=0.3)
      self.add(head1)
      elg=ErrLogGraph()
      self.add(elg)#
      self.wait_till("31:15")
      self.clear()
      #
      #
      #
      head1=Tex(r"Calculating only One Quadrant").to_edge(UP,buff=0.3)
      self.add(head1)
      self.wait_till("31:35")
      listing1 = Code(code=dedent("""
      N=200
      def fourshiftq(rin):
        rout=np.zeros(np.shape(rin))
        rout[1:,:] += rin[:-1,:]
        rout[:-1,:] += rin[1:,:]
        rout[:,1:] += rin[:,:-1]
        rout[:,:-1] += rin[:,1:]
        rout[0,:] += rin[1,:]
        rout[:,0] += rin[:,1]
      return rout
      """),font_size=32,line_spacing=1,language="Python",line_no_from=60)
      self.add(listing1)
      self.wait_till("32:48")
      self.remove(listing1)
      listing1 = Code(code=dedent("""
      Iq=np.zeros((N,N))
      Iq[0,0]=-2
      ir=2.0/(8*(N-1))
      Iq[-1,:]=ir
      Iq[:,-1]=ir
      """),font_size=32,line_spacing=1,language="Python",line_no_from=70)
      self.add(listing1)
      self.wait_till("33:40")
      #
      #
      #
      #
      #
      self.clear()
      head1=Tex(r"Using Conventional Linear Algebra").to_edge(UP,buff=0.3)
      self.add(head1)
      self.wait_till("34:12")
      tx1=Tex(r"$ \vec{\mathbf{I}} =  \mathbf{A} \vec{\mathbf{F}} $",font_size=64).next_to(head1,DOWN,buff=0.5).to_edge(LEFT,buff=0.5)
      self.play(Write(tx1),run_time=3.0)
      self.add(tx1)
      self.wait_till("34:42")
      tx2=Tex(r"$ \vec{\mathbf{F}} =  \mathbf{A}^{-1} \vec{\mathbf{I}} $",font_size=64).next_to(tx1,DOWN,buff=0.5).to_edge(LEFT,buff=0.5)
      self.play(Write(tx2),run_time=3.0)
      self.add(tx2)
      self.wait_till("35:37")
      tx3=Tex(r"$ \vec{\mathbf{I}} =  \begin{pmatrix} I_{0,0} \\ I_{0,1} \\ I_{0,2} \\ \vdots \\ I_{0,N} \\ I_{1,0} \\ \vdots \\ I_{N,N} \end{pmatrix} \;  $",font_size=48).next_to(tx1,DOWN,buff=0.3).to_edge(RIGHT,buff=1.5)
      self.play(Write(tx3),run_time=3.0)
      self.add(tx3)
      self.wait_till("37:05")
      tx4=Tex(r"For N=200 \\ $ \vec{\mathbf{I}} $ \ldots 40 000 Elements \\ $ \vec{\mathbf{F}} $ \ldots 40 000 Elements \\ $\mathbf{A} $ \ldots 800 000 000 Elemnts",font_size=48).next_to(tx2,DOWN,buff=1.0).to_edge(LEFT,buff=0.5)
      self.play(Write(tx4),run_time=3.0)
      self.add(tx4)
      self.wait_till("40:03")
      #
      #
      #
      self.clear()
      listing1 = Code(code=dedent("""
      import numpy as np
      from scipy.sparse import dok_matrix,
                  lil_matrix,linalg,
                  csr_matrix,csc_matrix
      N=200

      def mkidx(i,j):
        global N
        return i*N+j
      """),font_size=32,line_spacing=1,language="Python",line_no_from=1).to_edge(LEFT,buff=0.2)
      self.add(listing1)
      self.wait_till("40:38")
      tx3.to_edge(RIGHT,buff=0.2)
      tx3.shift(UP)
      self.play(Write(tx3),run_time=3.0)
      self.add(tx3)
      self.wait_till("41:41")
      #
      #
      self.clear()
      listing1 = Code(code=dedent("""
        Is=np.zeros((N*N,))
        Is[mkidx(0,0)]=2.0
        ir=-2.0/(8*(N-1))
        for i in range(N):
            Is[mkidx(i,N-1)]=ir
            Is[mkidx(N-1,i)]=ir
      """),font_size=32,line_spacing=1,language="Python",line_no_from=10).to_edge(LEFT,buff=0.2)
      self.add(listing1)
      self.wait_till("43:19")
      #
      self.clear()
      #
      #
      #
      listing1 = Code(code=dedent("""
        def La():
          global N
          N2=N*N
          sa=dok_matrix((N2, N2))
          for i in range(N-1):
            for j in range(N):
                sa[mkidx(i+1,j),mkidx(i,j)  ] +=1
                sa[mkidx(i,j),  mkidx(i+1,j)] +=1      
      """),font_size=32,line_spacing=1,language="Python",line_no_from=17).to_edge(LEFT,buff=0.2)
      self.add(listing1)
      self.wait_till("45:42")
      #
      #
      self.clear()
      listing1 = Code(code=dedent("""
        for i in range(N):
          for j in range(N-1):
              sa[mkidx(i,j+1),mkidx(i,j)  ] +=1
              sa[mkidx(i,j),  mkidx(i,j+1)] +=1
        for j in range(N):
            sa[mkidx(0,j), mkidx(1,j)] += 1 
            sa[mkidx(j,0), mkidx(j,1)] += 1 
      """),font_size=32,line_spacing=1,language="Python",line_no_from=25).to_edge(LEFT,buff=0.2)
      self.add(listing1)
      self.wait_till("46:28")
      #
      self.clear()
      tx1=Tex(r"$ \mathbf{L} F_{m,n} =  -4 \cdot F_{m,n}+F_{m+1,n}+F_{m-1,n}+F_{m,n+1}+F_{m,n-1} = -I_{m,n} $",font_size=42).to_edge(UP,buff=0.3).to_edge(LEFT,buff=0.3)
      self.play(Write(tx1),run_time=3.0)
      self.add(tx1)
      listing1 = Code(code=dedent("""
        for i in range(N):
          for j in range(N):
              zzz=4 if i < N and j < N else 
                     3 if i < N or j < N 
                       else 2
                sa[mkidx(i,j), mkidx(i,j)] -= zzz
        return sa      
      """),font_size=32,line_spacing=1,language="Python",line_no_from=32).to_edge(LEFT,buff=0.2)
      self.add(listing1)
      self.wait_till("47:35")
      #
      self.clear()
      tx1=Tex(r"$  \mathbf{A} \vec{\mathbf{x}} = \vec{\mathbf{I_s}} $",font_size=64).to_edge(UP,buff=0.5).to_edge(LEFT,buff=2.5)
      self.add(tx1)
      listing1 = Code(code=dedent("""
        A=csc_matrix(La())
        x=linalg.spsolve(A,Is)
        x -= x[0]
        print("R11=",x[mkidx(1,1)])
      """),font_size=32,line_spacing=1,language="Python",line_no_from=45).to_edge(LEFT,buff=0.2)
      self.add(listing1)
      self.wait_till("50:08")
      #
      self.clear()
      tx1=Text("""N=1000\nR11=0.6366197723676748,  R01=R10=0.5 \n
\nR11*pi*0.5= 1.0000000000001468\nR11err= 1.467714838554457e-13\n
runtime=77.812 seconds""",font_size=32,font="Monospace").to_edge(UP,buff=1).to_edge(LEFT,buff=1)
      self.add(tx1)
      
      self.wait_till("50:59")
