from manim import *
import numpy as np
#from manim.imports import * 
from manim_physics import *
import lcapy


class MultiMeter(VGroup):
   def __init__(self,pos=None,red=None,black=None,rtred=35,rtblack=-35,disp=None):
        super().__init__()
        self.pos=pos
        mm = SVGMobject("multimeter.svg",stroke_width=0.0).scale(1.5)
        rt=  SVGMobject("redtip.svg",stroke_width=0.0)
        bt=  SVGMobject("blacktip.svg",stroke_width=0.0)
        if pos:
          mm.move_to(pos)
        self.add(mm)
        if red:
          rt.move_to(red)
          rt.shift(rt.height*DOWN*0.5)
          rtarget=rt.get_top() + [rt.height*np.sin(rtred*DEGREES),-rt.height*np.cos(rtred*DEGREES),0]
          rt.rotate(rtred*DEGREES,about_point=rt.get_top())
          #t1=red.get_top()
          #delta=rt.get_top()-t1
          #rt.move_to(UP*delta)
          self.add(rt)
          rstart=mm.get_bottom()+RIGHT*0.6+UP*0.57
          bezier = CubicBezier(rstart, rstart + 1 * RIGHT, rtarget + [rt.height*np.sin(rtred*DEGREES),-rt.height*np.cos(rtred*DEGREES),0] , rtarget, stroke_width=6)
          bezier.color="#A00000"
          self.add(bezier)
          
        if black:
          bt.move_to(black)
          bt.shift(bt.height*DOWN*0.5)
          btarget=bt.get_top() + [bt.height*np.sin(rtblack*DEGREES),-bt.height*np.cos(rtblack*DEGREES),0]
          bt.rotate(rtblack*DEGREES,about_point=bt.get_top())
          #t2=black.get_top()
          #delta=bt.get_top()-t2
          #bt.move_to(UP*delta)  
          self.add(bt)
          bstart=mm.get_bottom()+RIGHT*0.6+UP*0.32
          bezier = CubicBezier(bstart, bstart - 1 * UP, btarget + [bt.height*np.sin(rtblack*DEGREES),-bt.height*np.cos(rtblack*DEGREES),0] , btarget, stroke_width=6)
          bezier.color="#000000"
          self.add(bezier)
        if disp:
          disptxt=Text(disp,font="DSEG14 Classic",font_size=20,color=BLACK)
          disptxt.move_to(mm.get_top() + DOWN*0.4+LEFT*0.15)
          self.disptxt=disptxt
          self.add(disptxt)
      






class Resistor(VGroup):
   def __init__(self,txt=None):
        super().__init__()
        self.txt=txt
        self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        self.body=Rectangle(height=0.5, width=0.15, stroke_width=2)
        self.body.shift(2*UP/4)
        for d in self.dots:
          self.add(d)
        self.add(self.body)
        if self.txt:
          self.tex = Tex(txt, font_size=12)
          self.tex.shift(UP/2+RIGHT/5)
          self.add(self.tex)  
        self.add(Line(self.dots[0],UP/4,stroke_width=2))
        self.add(Line(UP*3/4,self.dots[1],stroke_width=2))


class scene1s1(MovingCameraScene):
    def wait_till(self,n):
      ns=n
      ts=self.timestamp
      if isinstance(n,str):
        fval=[1,60,60]
        nfields=n.split(":")
        n=sum([fval*int(nf) for fval,nf in zip(fval,nfields[::-1])])
        print("waitting till",n,ns)
      tdelta=n-self.renderer.time-ts
      if tdelta < 0:
        print("============= BEHIND SCHEDULE ==============")
      elif tdelta > 0:  
        self.wait(tdelta)
        
    def construct(self):
      self.timestamp=0
      self.camera.background_color = '#403800'
      # r1 = drawgrid(10)
      # r1.draw(style='european',scale=3,filename="tmp0s1.svg",color='white')
      chap1=Tex(r"{\LARGE Chapter I} \\ Electrical Resistance and Simple Electrical Networks").move_to([0, 3, 0])
      self.play(Write(chap1))
      self.add(chap1)
      self.wait(1)
      btext,btime=(r"Basics of Electricity: Voltage, Potential, Current", "00:07")
      print("adding,",btext,"at",btime)
      self.wait_till(btime)
      btex=Tex(btext)
      self.play(Write(btex))
      self.add(btex)
      self.play(Indicate(btex),runt_time=2.0)
      self.wait_till("00:30")
      self.clear()
      btext=r"{\Large What drives electricity?}"
      btex=Tex(btext).move_to([0, 3, 0])
      self.play(Write(btex))
      self.wait_till("00:50")
      self.remove(btex)
      d1=Dot().move_to([-4,2,0])
      d2=Dot().move_to([4,2,0])
      self.add(d1)
      self.add(d2)
      self.add(Tex(r"0V").move_to(d1.get_top()+UP*0.2))
      self.add(Tex(r"12V").move_to(d2.get_top()+UP*0.2))
      
      dista=DoubleArrow(d1, d2, buff=0,stroke_width=0.3,max_tip_length_to_length_ratio=0.01)
      dista=VGroup(dista,Tex(r"\small Distance 20mm").move_to(dista.get_top() + UP*0.2))
      self.play(FadeIn(dista),run_time=5.0)
      self.add(dista)
      va=Arrow(d2.get_top()+DOWN*1.5, d1.get_top()+DOWN*1.5, buff=0,stroke_width=1,max_tip_length_to_length_ratio=0.2)
      va=VGroup(va,Tex(r"\small $ E \approx \frac{12V}{20mm} = \frac{0.6V}{1mm} = 600 \frac{V}{m}$").move_to(va.get_top() + UP*0.2))
      self.play(FadeIn(va),run_time=2.0)
      self.add(va)
      self.wait_till("01:19")
      self.remove(dista)
      qt=Dot(color=BLUE).move_to([0,2,0])
      qtxt=Tex(r"\tiny test charge").move_to(qt.get_top() + UP*0.2)
      self.add(qt,qtxt)
      force=Arrow(LEFT*0.2+UP*2,LEFT*0.5+UP*2, buff=0,color=GOLD)
      self.add(force)
      self.wait(1)
      self.remove(force,qt,qtxt)
      self.wait_till("01:34")
      electron=Dot(color=BLUE).move_to([0,2,0])
      electron=VGroup(qt,Tex(r"\tiny $ q_{e^{-}} = -1.6 \cdot  10^{-19} As$").move_to(electron.get_top() + UP*0.2))
      eforce=Arrow(RIGHT*0.2+UP*2,RIGHT*0.5+UP*2, buff=0,color=GOLD)
      self.add(electron)
      self.add(eforce)
      #
      #btex=Tex(btext).move_to([0, 0, 0])
      #self.play(Write(btex),run_time=10)
      self.wait_till("02:13") #  Ω
      mume=MultiMeter(Point([-4,-2,0]),d2,d1,disp="12.0V")
      self.add(mume)
      self.wait_till("02:31")
      self.remove(mume)
      mume=MultiMeter(Point([-4,-2,0]),d1,d2,disp=" -12V")
      self.play(FadeIn(mume))
      self.add(mume)
      # self.play(Indicate(btex[0][10:30]),run_time=5)
      self.wait_till("03:57")
      #
      #
      self.clear()
      d1=Dot().move_to([-3,1,0])
      d2=Dot().move_to([3,3,0])
      d3=Dot().move_to([1,-1,0])
      self.add(d1,d2,d3)
      self.add(Text("A").move_to(d1.get_top()+0.4*UP),
               Text("B").move_to(d2.get_top()+0.4*UP),
               Text("C").move_to(d3.get_top()+0.4*UP))
      
      self.add(Tex("\small $ U_A=3V $").move_to(d1.get_top()+1.2*RIGHT),
               Tex("\small $ U_B=7V $").move_to(d2.get_top()+1.2*RIGHT),
               Tex("\small $ U_C=-2V $").move_to(d3.get_top()+1.3*RIGHT))

      learth = lcapy.Circuit()
      learth.add("""
      W 2_0 3_0; down=0.1, ground
      """)
      learth.draw(style='european',scale=4,filename="earth.svg",color='white')
      earth=SVGMobject("earth.svg",stroke_width=1).scale(0.7)
      earth.move_to([4,-2.5,0])
      self.add(earth)
      self.add(Tex("\small $0V$ (Ground)").move_to(earth.get_top()+1.1*RIGHT+0.5*DOWN))
      self.wait_till("04:54")
      mume=MultiMeter(Point([-4,-2,0]),d2,Point(earth.get_top()),disp=" 7.0V",rtblack=270)
      self.add(mume)
      self.wait_till("05:02")
      self.remove(mume)
      mume=MultiMeter(Point([-4,-2,0]),d2,d1,disp=" 4.0V")
      self.add(mume)
      self.wait_till("05:15")
      self.remove(mume)
      #Uba=Arrow(d2,d1,color=BLUE)
      Uba=ArcBetweenPoints(d2.get_top(),d1.get_top(),color=BLUE)
      Uba.add_tip()
      #Ubc=Arrow(d2,d3,color=BLUE)
      Ubc=ArcBetweenPoints(d2.get_top(),d3.get_top(),color=BLUE)
      Ubc.add_tip()
      #Uac=Arrow(d1,d3,color=BLUE)
      Uac=ArcBetweenPoints(d1.get_top(),d3.get_top(),color=BLUE)
      Uac.add_tip()
      Ucb=ArcBetweenPoints(d3.get_top(),d2.get_top(),color=BLUE)
      Ucb.add_tip()
      self.add(Uba)
      self.add(Tex(r"$ U_{BA}= 4V $",color=BLUE).move_to(Uba.get_center()+0.6*UP))
      self.wait_till("05:35")
      self.add(Ubc)
      texbc=Tex(r"$ U_{BC}= 9V $",color=BLUE).move_to(Ubc.get_center()+1.1*RIGHT)
      self.add(texbc)
      self.wait_till("05:42")
      self.add(Uac)
      texac=Tex(r"$ U_{AC}= 5V $",color=BLUE).move_to(Uac.get_center()+0.8*DOWN)
      texcb=Tex(r"$ U_{CB}= -9V $",color=BLUE).move_to(Ucb.get_center()+0.8*DOWN)
      self.add(texac)
      self.wait_till("05:51")
      self.play(FadeOut(texbc),FadeOut(Ubc))
      self.remove(texbc,Ubc)
      self.play(FadeIn(texcb),FadeIn(Ucb))
      self.add(texcb,Ucb)
      self.wait_till("06:10")
      self.camera.frame.save_state()
      self.play(self.camera.frame.animate.set(width=20),run_time=2.0)
      self.play(self.camera.frame.animate.move_to(Point([-3.0,3.0,0])))
      addall=Tex(r"$ U_{BA} + U_{AC} + U_{CB} = $").move_to([-5,6.0,0])
      self.add(addall)
      self.wait(5)
      addall2=Tex(r"$ = (U_{B} - U_{A} ) + (U_{A} - U_{C} ) +  (U_{C} - U_{B} ) = 0 $").move_to(addall.get_bottom()+0.6*DOWN)
      self.add(addall2)
      self.wait(5)
      self.add(Text("Kirchhoff's Loop Rule").move_to([-5,7,0])) 
      self.wait(5)
      self.play(Restore(self.camera.frame),run_time=1.0)
      self.wait_till("06:55")
      self.clear()
      d1=Dot().move_to([0,0,0])
      d2=Dot().move_to([0,3,0])
      #self.add(d1)
      #self.add(d2)
      a1=Arrow([0,4,0],[0,0,0])
      a1.index=2
      self.add(a1)
      charges=[Dot(color=BLUE).scale(0.7).move_to([0.2,4.0*i/10.0,0]) 
                 for i in range(10)]
      for dot in charges:
        dot.rate_func=rate_functions.linear
      lastdot=charges[0]           
      self.add(*charges)           
      self.wait(5)
      for i in range(20):
         #self.play(vcharge.animate.move_to([0,-0.4,0]),FadeOut(lastdot))
         movesanim=[ApplyMethod(dot.shift,DOWN*0.4,rate_func=rate_functions.linear) for dot in charges]
         self.play(*movesanim,FadeOut(lastdot),run_time=1.0)
         for j in range(10):
           charges[j].move_to([0.2,4.0*j/10.0,0])
         self.play(FadeIn(lastdot),run_time=0)
         if i==5:
           dline=DashedLine([-2,2,0], [2,2,0], dash_length=0.1,color=RED)
           self.add(dline)
         if i==10:
           self.add(Tex(r"$ I=\frac{N * q}{second} = \frac{1As}{s} = 1A $").move_to([-4,2.5,0]) )  
           self.add(Tex(r"$ N $\ldots Number of Charges").move_to([-4,1.5,0]))
           self.add(Tex(r"$ q $\ldots Charge in As").move_to([-4,0.5,0]))
      self.wait(10)
      self.remove(*charges)
      Ia=Arrow([0,2,0],[0,1,0],stroke_width=3.0,stroke_color=RED,max_tip_length_to_length_ratio=2.0)
      Ia.index=4
      #self.remove(a1)
      self.remove(dline)
      self.play(FadeIn(Ia))
      self.add(Ia)
      self.add(Tex(r"$ I=1A $",color=RED).move_to([1,1.6,0]))
      self.wait_till("07:22")
      self.clear()
      # kirchhoff2
      self.add(d1.scale(3))
      points=np.array([[0,6,0], [-3,-6,0] , [5,-6,0], [10,10,0] ])
      amps= [            5,      -3,       -4,            2 ]
      lines=[ Line(d1, pnt) for pnt in points]
      self.add(*lines)
      ndx=0
      for a,pnt in zip(amps,points):
        ndx += 1
        print(pnt,pnt*pnt)
        plen=np.sqrt(sum(pnt*pnt))
        print("len",plen)
        pdir=pnt/plen
        if a < 0:
          amp=Arrow(pdir  , pdir*2,stroke_width=3.0,stroke_color=RED,max_tip_length_to_length_ratio=2.0)
        else:
          amp=Arrow(2*pdir  , pdir,stroke_width=3.0,stroke_color=RED,max_tip_length_to_length_ratio=2.0)  
        self.add(Tex(r"$ I_{" + str(ndx) +"}=" + f"{abs(a)}" +" mA $",color=RED).move_to(2.1*pdir))
        self.add(amp)
      self.wait(10)
      self.add(Tex(r"\large $ \sum I_{in} = \sum I_{out}$ ").move_to([-4,2,0]))
      self.wait(5)
      self.add(Tex(r"$ I_1 + I_4  = I_2 + I_3 $ ").move_to([-4,0,0]))
      self.wait(5)
      self.add(Text(r"Kirchhoff's Current Law").move_to([-1,3.5,0]))
      self.wait_till("08:02")
      

      
      
      
      