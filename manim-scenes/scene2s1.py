from manim import *
import numpy as np
#from manim.imports import * 
from manim_physics import *
import lcapy

def myscale(obj,fact):
  # scale around origin
  c=obj.get_center()
  obj.scale(fact)
  obj.shift((fact-1)*c)
  return obj

def mkcoordmn(m,n):
  if m < 0:
    coord=f"m-{abs(m)},"
  elif m > 0:
    coord=f"m+{abs(m)},"
  else:
    coord=f"m,"  
  if n < 0:
    coord += f"n-{abs(n)}"
  elif n > 0:
    coord += f"n+{abs(n)}"
  else:
    coord += f"n"  
  coord=coord.replace('-',r'‒') #unicode minus
  return coord

def mkcoord(m,n):
  if m < 0:
    coord=f"-{abs(m)},"
  elif m > 0:
    coord=f"{abs(m)},"
  else:
    coord=f"0,"  
  if n < 0:
    coord += f"-{abs(n)}"
  elif n > 0:
    coord += f"{abs(n)}"
  else:
    coord += f"0"  
  coord=coord.replace('-',r'‒') #unicode minus
  return coord


class FunSequence(VGroup):
    def __init__(self,seq=None,fun=None,start=-2,stop=12,scale=0.5,xscale=0.7):
        super().__init__()
        self.fun=fun
        self.seq=seq
        self.scale=scale
        self.xscale=xscale
        if not seq:
          self.seq=[]
          seq=self.seq
        if fun:
          for i in range(start,stop,1):
            self.seq.append(fun(i))
        print("seq start=",start,"stop=",stop,"seq=",seq)
        self.offset=-2
        self.baseline=Line([start+self.offset,0,0],[stop+self.offset,0,0],color=GREEN)
        self.add(self.baseline)
        for i in range(start,stop,1):
          color = WHITE if i >= 0 else RED
          self.add(Dot([xscale*i+self.offset,0,0],color=color))
          self.add(Text(str(i),font_size=12,color=color).move_to([xscale*i+self.offset+0.1,0.1,0]))
          val=seq[i-start]
          if val > 0 or i >= 0:
            dt=Dot([xscale*i+self.offset,val*scale,0],color=GREEN_E)
            self.add(dt)
            self.add(Line([xscale*i+self.offset,0,0],dt.get_bottom(),stroke_width=0.5))
            sval=str(round(val,3))
            fsize=24
            if len(sval)>2:
              fsize=16
              if len(sval)>4:
                fsize=12
            self.add(Text(sval,font_size=fsize).move_to([xscale*i+self.offset+0.2,val*scale+0.2,0]))
  

class MultiMeter(VGroup):
   def __init__(self,pos=None,red=None,black=None,rtred=35,rtblack=-35,disp=None):
        super().__init__()
        self.pos=pos
        mm = SVGMobject("multimeter.svg",stroke_width=0.0).scale(1.5)
        rt=  SVGMobject("redtip.svg",stroke_width=0.0)
        bt=  SVGMobject("blacktip.svg",stroke_width=0.0)
        self.mm=mm
        if pos:
          mm.move_to(pos)
        self.add(mm)
        if red:
          rt.move_to(red)
          rt.shift(rt.height*DOWN*0.5)
          rtarget=rt.get_top() + [rt.height*np.sin(rtred*DEGREES),-rt.height*np.cos(rtred*DEGREES),0]
          rt.rotate(rtred*DEGREES,about_point=rt.get_top())
          #t1=red.get_top()
          #delta=rt.get_top()-t1
          #rt.move_to(UP*delta)
          self.add(rt)
          rstart=mm.get_bottom()+RIGHT*0.6+UP*0.57
          bezier = CubicBezier(rstart, rstart + 1 * RIGHT, rtarget + [rt.height*np.sin(rtred*DEGREES),-rt.height*np.cos(rtred*DEGREES),0] , rtarget, stroke_width=6)
          bezier.color="#A00000"
          self.add(bezier)
          
        if black:
          bt.move_to(black)
          bt.shift(bt.height*DOWN*0.5)
          btarget=bt.get_top() + [bt.height*np.sin(rtblack*DEGREES),-bt.height*np.cos(rtblack*DEGREES),0]
          bt.rotate(rtblack*DEGREES,about_point=bt.get_top())
          #t2=black.get_top()
          #delta=bt.get_top()-t2
          #bt.move_to(UP*delta)  
          self.add(bt)
          bstart=mm.get_bottom()+RIGHT*0.6+UP*0.32
          bezier = CubicBezier(bstart, bstart - 1 * UP, btarget + [bt.height*np.sin(rtblack*DEGREES),-bt.height*np.cos(rtblack*DEGREES),0] , btarget, stroke_width=6)
          bezier.color="#000000"
          self.add(bezier)
        if disp:
          disptxt=Text(disp,font="DSEG14 Classic",font_size=20,color=BLACK)
          disptxt.move_to(mm.get_top() + DOWN*0.4+LEFT*0.15)
          self.disptxt=disptxt
          self.add(disptxt)
          
   def update_text(self,newtext):
      disptxt=self.disptxt
      newdisp=Text(newtext,font="DSEG14 Classic",font_size=20,color=BLACK)
      newdisp.move_to(self.mm.get_top() + DOWN*0.4+LEFT*0.15)
      return Transform(disptxt,newdisp)

class Resistor(VGroup):
   def __init__(self,txt=None):
        super().__init__()
        self.txt=txt
        self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        self.body=Rectangle(height=0.5, width=0.15, stroke_width=2)
        self.body.shift(2*UP/4)
        for d in self.dots:
          self.add(d)
        self.add(self.body)
        if self.txt:
          self.tex = Tex(txt, font_size=12)
          self.tex.shift(UP/2+RIGHT/5)
          self.add(self.tex)  
        self.add(Line(self.dots[0],UP/4,stroke_width=2))
        self.add(Line(UP*3/4,self.dots[1],stroke_width=2))

class Battery(VGroup):
   def __init__(self,txt=None):
        super().__init__()
        self.txt=txt
        self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        for d in self.dots:
          self.add(d)
        self.add(Line(self.dots[0],UP*2/5,stroke_width=2))
        self.add(Line(UP*3/5,self.dots[1],stroke_width=2))
        lplus=Line(UP*2/5+LEFT*0.2,UP*2/5-LEFT*0.2,stroke_width=2)
        self.add(lplus)
        self.add(Line(UP*3/5+LEFT*0.3,UP*3/5-LEFT*0.3,stroke_width=2))
        self.add(Text("+",font_size=18).move_to(UP*3.3/5+LEFT*0.2))
        self.add(Text("-",font_size=18).move_to(UP*1.7/5+LEFT*0.2))
        if self.txt:
          self.tex = Tex(txt, font_size=24)
          self.tex.shift(UP/2) #
          self.tex.next_to(lplus,LEFT)
          self.add(self.tex)  


class CurrentSource(VGroup):
   def __init__(self,txt=None):
        super().__init__()
        self.txt=txt
        self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        for d in self.dots:
          self.add(d)
        circ=Circle(radius=1.0/4.0,color=WHITE,stroke_width=2).shift(UP*1.0/2.0)
        self.add(circ)
        self.add(Line(self.dots[0],UP*1/4,stroke_width=2))
        self.add(Line(UP*3/4,self.dots[1],stroke_width=2))
        self.add(Arrow( [0,1.3/4.0,0], [0,2.7/4,0] ))
        if self.txt:
          self.tex = Tex(txt, font_size=24)
          self.tex.shift(UP/2).next_to(circ,LEFT)
          self.add(self.tex)  


class Short(VGroup):
   def __init__(self,replace=None):
        super().__init__()
        if replace:
          self.dots=replace.dots.copy()
        else:
          self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        for d in self.dots:
          self.add(d)
        self.add(Line(self.dots[0],self.dots[1],stroke_width=2))


class scene2s1(MovingCameraScene):
    def wait_till(self,n):
      ns=n
      ts=self.timestamp
      if isinstance(n,str):
        fval=[1,60,60]
        nfields=n.split(":")
        n=sum([fval*int(nf) for fval,nf in zip(fval,nfields[::-1])])
        print("waitting till",n,ns)
      tdelta=n-self.renderer.time-ts
      if tdelta < 0:
        print("============= BEHIND SCHEDULE ==============")
      elif tdelta > 0:  
        self.wait(tdelta)
        
    def construct(self):
      self.timestamp=0
      self.camera.background_color = '#302800'
      # r1 = drawgrid(10)
      # r1.draw(style='european',scale=3,filename="tmp0s1.svg",color='white')
      if True:
        chap1=Tex(r"{\LARGE Chapter II} \\ Taming Infinity with Generating Functions \\ and Z-Transform").move_to([0, 3, 0])
        self.play(Write(chap1))
        self.add(chap1)
        self.wait_till("00:10")
        self.add(Tex(r"But First let's look at our original problem once more").move_to([0, 1, 0]))
        self.wait_till("00:20")
        self.clear()
        #self.add(Dot([0,0,0],color=RED,radius=0.1),Dot([1,0,0],color=BLUE,radius=0.1),Dot([0,1,0],color=GREEN,radius=0.1))
        mright=3.0
        reslist=[]
        for d in range(4):
          R1=Resistor(r"$ 1\Omega $")
          myscale(R1,3)
          #R1.move_to(R1.dots[0].get_center())
          R1.rotate_about_origin(d*90*DEGREES)
          R1.shift(mright*RIGHT)
          self.add(R1)
          reslist.append(R1)
        ulab=[]
        self.wait_till("00:30")  
        for i in range(-1,2):
            for j in range(-1,2):
              if (abs(i)+abs(j) < 2):
                coord=f"{i},{j}"
                coord=coord.replace('-',r'‒') #unicode minus
                tl=Tex(r"$U_{" + coord + "}$",font_size=28).move_to([j*3-0.4+mright,i*3+0.2,0])
                ulab.append(tl)
                self.add(tl)
        self.wait_till("00:40")
        self.remove(*ulab)        
        ulab=[]  
        for i in range(-1,2):
            for j in range(-1,2):
              if (abs(i)+abs(j) < 2):
                coord=mkcoordmn(i,j)
                tl=Tex(r"$U_{" + coord + "}$",font_size=28).move_to([j*3-0.45+mright,i*3+0.3,0])
                ulab.append(tl)
                self.add(tl)
        self.wait(2)
        t1=Tex(r"\tiny $ I_{m,n} =  0  $ \tiny \hspace{0.1cm} $ \forall m \neq 0 \lor n \neq 0$").move_to([-4.4,3.5,0])
        t2=Tex(r"\tiny $ I_{0,0} = -1 $").next_to(t1,DOWN).align_to(t1,LEFT)
        t3=Tex(r"\tiny $ I_{m,n} = I_{norh}+I_{south}+I_{west}+I_{east} $").next_to(t2,DOWN).align_to(t2,LEFT)
        t4=Tex(r"\tiny $  = \frac{1}{1\Omega} \cdot ( (U_{m,n} - U_{m+1,n}) +  \ldots  $").next_to(t3,DOWN).align_to(t3,LEFT)
        t5=Tex(r"\tiny $  = 4\cdot U_{m,n} - U_{m+1,n} -U_{m-1,n} - U_{m,n+1} - U_{m,n-1} $").next_to(t4,DOWN).align_to(t4,LEFT)
        self.add(t1)
        self.wait(1)
        self.add(t2)
        self.wait(1)
        self.add(t3)
        self.wait_till("00:44")
        self.add(t4)
        self.wait_till("01:05")
        self.add(t5)
        #self.play(ReplacementTransform(t1,t2))
        self.wait_till("01:50")
        self.remove(*ulab)
        self.remove(*reslist)
        self.wait_till("02:07")
        ulab=[]
        for i in range(-1,2):
            for j in range(-1,2):
              if (abs(i)+abs(j) < 2):
                coord=mkcoordmn(i,j)
                #tl=Tex(r"$U_{" + coord + "}$",font_size=28).move_to([j*3-0.45+mright,i*3+0.3,0])
                nfac=4 if i==0 and j==0 else -1
                tl=Text(str(nfac),font_size=28).move_to([j*1-0.45+mright,i*1+0.3-2,0])
                ulab.append(tl)
                self.add(tl)
        self.wait_till("02:22")
        self.clear()
        numplane=NumberPlane(x_range=(-3, 9, 1), y_range=(-3, 4, 1))
        self.add(numplane)
        for m in range(-3,4,1):
          for n in range(-3,9,1):#
            coord=mkcoord(m,n)
            dt=Dot(numplane.coords_to_point(n,m),radius=0.07)
            self.add(Tex(r"$ F_{" + coord + "} $",font_size=20).align_to([0.1,0.1,0]+numplane.coords_to_point(n,m),LEFT+DOWN))
            self.add(dt)
        self.wait_till("02:32")
        t6=Tex(r"$ -4\cdot F_{m,n} + F_{m+1,n} +F_{m-1,n} + F_{m,n+1} + F_{m,n-1} = I_{m,n} $",font_size=32).move_to([0,2,0])
        t6bg=BackgroundRectangle(t6, color="#302800", fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.4)
        t7=Tex(r"$ \nabla^{2} F_{m,n} = -4*F_{m,n}+F_{m+1,n}+F_{m-1,}+F_{m,n+1}+F_{m,n-1} = \mathbf{L} F_{m,n}$",font_size=32).move_to([0,1,0])
        t7bg=BackgroundRectangle(t7, color="#302800", fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.4)
        self.add(t6bg,t6)
        self.wait_till("02:41")
        self.clear()
        self.add(t6)
        self.add(t7bg,t7)
        self.wait_till("04:10")
        self.clear()
        headline=Tex(r"Let's Focus on 1D Difference Equations First \\ Example: Fibonacci Sequence").move_to([0, 3, 0])
        self.play(Write(headline),run_time=3.0)
        self.add(headline)
        self.wait_till("04:34")
        fibtex=Tex(r"$ F_{n+2}=F_{n}+F_{n+1} $").move_to([0,1.5,0])
        fibtex2=Tex(r"\tiny $ F_0=0, F_1 = 1 $").move_to([0,0.5,0])
        self.add(fibtex)
        self.wait_till("05:11")
        self.add(fibtex2)
        self.wait_till("05:27")
        self.clear()
        headline=Tex(r"How do we represent a sequence of numbers in mathematics?").move_to([0, 3, 0])
        self.play(Write(headline),run_time=3.0)
        self.add(headline)
        vtxt=Text("We could use a Vector",font_size=18).move_to([-4,1,0])
        self.add(vtxt)
        myseq=[4, 1, 2, 0, 11]
        myv1=np.array([ myseq ]).T
        v1 = Matrix(myv1).scale(0.4).next_to(vtxt,RIGHT)
        self.wait_till("05:37")
        self.add(v1)
        ptxt=Text("We could use a Polynomial",font_size=18).move_to([-4,-1,0])
        self.add(ptxt)
        p1 = Tex(r"$p(x)=4+x+2x^2+0x^3+11x^4$").next_to(ptxt,RIGHT)
        self.wait(5)
        self.add(p1)
        self.wait(5)
        self.remove(v1,vtxt)
        p1.shift(3*UP)
        ptxt.shift(3*UP)
        p2 = Tex(r" if we want to shift the values to the right: \\ $ x \cdot p(x)=4x+x^2+2x^3+0x^4+11x^5$").next_to(p1,DOWN).align_to(ptxt,LEFT)
        self.add(p2)
        self.wait_till("06:57")
        p3 = Tex(r" if we want to shift the values to the left: \\ $ p(x)/x=4/x+1+2x+0x^2+11x^3$").next_to(p2,DOWN).align_to(p2,LEFT)
        self.add(p3)
        self.wait(5)
        p4 = Tex(r" represent an infinit sequence: \\ $ f(x)=\sum\limits_{n=0}^{\infty} a_n \cdot x^n$").next_to(p3,DOWN).align_to(p3,LEFT)
        self.add(p4)
        self.wait_till("08:17")
        #
        #
        self.clear()
        fs1=FunSequence(scale=2,fun=lambda n: 1 if n >= 0 else 0)
        self.add(fs1)
        self.add(Text("The Simplest Sequence: All 1").move_to([0,3,0]))
        self.wait_till("08:57")
        tx1=Tex(r"$ f(x) = 1 + x + x^2 + x^3 +x^4 + x^5 + \ldots $").next_to(fs1,DOWN)
        self.add(tx1)
        self.wait(5)
        tx2=Tex(r"$ F_n = 1 $").next_to(fs1,LEFT).shift(UP)
        tx2.index=2
        self.add(tx2)
        self.wait(5)
        tx3=Tex(r"$ x \cdot f(x) = x + x^2 +x^3 + \ldots$").next_to(tx1,DOWN)
        tx3.shift(LEFT*(tx1[0][7].get_center()[1]-tx3[0][7].get_center()[1]))
        self.add(tx3)
        self.wait(5)
        for q in tx1[0][7:]:
          q.color=GREEN
        for q in tx3[0][7:]:
          q.color=GREEN
        self.wait_till("09:25")  
        tx4=Tex(r"$ f(x) - x \cdot f(x) = 1 $").next_to(tx3,DOWN).align_to(tx1,LEFT)
        self.add(tx4)
        self.wait_till("09:40")
        tx5=Tex(r"$ f(x) = \frac{1}{1-x} $").next_to(tx4,DOWN).align_to(tx1,LEFT)
        tx5.index=2
        tx6=tx5.copy()
        tx6.next_to(tx2,DOWN).align_to(tx2,LEFT)
        self.add(tx5,tx6)
        self.wait(5)
        bg6=BackgroundRectangle(tx6, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
        bg2=BackgroundRectangle(tx2, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
        self.remove(tx2,tx6)
        self.add(bg2,bg6)
        self.add(tx2,tx6)
        self.wait_till("10:00")
        trafo=ArcBetweenPoints(bg2.get_left(),bg6.get_left(),color=PURPLE_E)
        trafo.add_tip()
        self.add(trafo)
        self.wait_till("10:20")
        # 
        #
        self.clear()
        fs1=FunSequence(scale=2,fun=lambda n: 1/2**n if n >= 0 else 0)
        self.add(fs1)
        self.add(Text("A Geometric Series").move_to([0,3,0]))
        self.wait_till("10:30")
        tx1=Tex(r"$ f(x) = 1 + \frac{1}{2} \cdot  x + \frac{1}{2^2} \cdot x^2 + \ldots + \frac{1}{2^n}\cdot x^n $").next_to(fs1,DOWN)
        self.add(tx1)
        self.wait_till("11:00")
        tx2=Tex(r"$ F_n = \frac{1}{2^n} $").next_to(fs1,LEFT).shift(UP)
        tx2.index=2
        self.add(tx2)
        self.wait(2)
        tx3=Tex(r"$ F_{n+1}=\frac{F_{n}}{2}$").next_to(tx1,DOWN)
        tx3.shift(LEFT*(tx1[0][7].get_center()[1]-tx3[0][7].get_center()[1]))
        self.add(tx3)
        self.wait_till("11:20")
        tx6=Tex(r"$ f(x) = \frac{1}{1-0.5 \cdot x} $").next_to(tx2,DOWN).align_to(tx2,LEFT)
        self.add(tx6)
        self.wait_till("11:27")
        bg6=BackgroundRectangle(tx6, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
        bg2=BackgroundRectangle(tx2, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
        self.remove(tx2,tx6)
        self.add(bg2,bg6)
        self.add(tx2,tx6)
        self.wait_till("11:42")
        #
        self.clear()
        fs1=FunSequence(scale=0.5,fun=lambda n: 1.2 **n if n >= 0 else 0)
        self.add(fs1)
        self.add(Text("A Geometric Series, c=1.2").move_to([0,3,0]))
        tx1=Tex(r"$ f(x) = 1 + c \cdot  x + c^2 \cdot x^2 + \ldots + c^n \cdot x^n $").next_to(fs1,DOWN)
        self.add(tx1)
        tx2=Tex(r"$ F_n = c^n $").next_to(fs1,LEFT).shift(UP)
        tx2.index=2
        self.add(tx2)
        self.wait(5)
        tx3=Tex(r"$ F_{n+1}= c \cdot F_{n} $").next_to(tx1,DOWN)
        tx3.shift(LEFT*(tx1[0][7].get_center()[1]-tx3[0][7].get_center()[1]))
        self.add(tx3)
        self.wait(5)
        tx6=Tex(r"$ f(x) = \frac{1}{1 - c \cdot x} $").next_to(tx2,DOWN).align_to(tx2,LEFT)
        self.add(tx6)
        bg6=BackgroundRectangle(tx6, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
        bg2=BackgroundRectangle(tx2, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
        self.remove(tx2,tx6)
        self.add(bg2,bg6)
        self.add(tx2,tx6)
        self.wait_till("12:15")
        #
        self.clear()
        self.add(Text("Sequence of Partial Sums").move_to([0,3,0]))
        tx1=Tex(r"$ f(x) = a_0 + a_1 \cdot  x + a_2 \cdot x^2 + \ldots + a_n \cdot x^n + \ldots $",font_size=36).move_to([-3,2.0,0])
        self.add(tx1)
        self.wait(3)
        tx2=Tex(r"$ g(x) = a_0  + (a_0+a_1) x  + (a_0 +a_1 +a_2) x^2 + \ldots + x^n \cdot \sum\limits_{k=0}^n a_k + \ldots      $",font_size=36).next_to(tx1,DOWN).align_to(tx1,LEFT)
        tx2.index=2
        self.add(tx2)
        self.wait(5)
        tx3=Tex(r"$ g(x) = \frac{1}{1-x} \cdot f(x) $",font_size=64).next_to(tx2,DOWN).align_to(tx2,LEFT)
        self.add(tx3)
        self.wait_till("13:22")
        bg3=BackgroundRectangle(tx3, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
        self.remove(tx3)
        self.add(bg3,tx3)
        self.wait_till("14:23")
        #
        self.clear()
        fs1=FunSequence(scale=0.2,fun=lambda n: n if n >= 0 else 0) #.next_to(bg3,DOWN).align_to(tx2,LEFT).shift(2*RIGHT)
        self.add(fs1)
        self.add(Text("The Sequence of Natural Numbers").move_to([0,3,0]))
        tx1=Tex(r"$ f(x) = 0 + 1 \cdot  x + 2 \cdot x^2 + \ldots + n \cdot x^n + \ldots $").next_to(fs1,DOWN)
        self.add(tx1)
        tx2=Tex(r"$ F_n = n $").next_to(fs1,LEFT).shift(UP)
        tx2.index=2
        self.add(tx2)
        self.wait(5)
        tx3=Tex(r"$ F_{n+1}= F_{n} + 1 $").next_to(tx1,DOWN).align_to(tx1,LEFT)
        tx3.shift(LEFT*(tx1[0][7].get_center()[1]-tx3[0][7].get_center()[1]))
        self.add(tx3)
        self.wait_till("14:23")
        tx5=Tex(r"$ f(x) = \frac{1}{1 - x}  \cdot \frac{1}{1-x} \cdot x = \frac{x}{(1-x)^2} $").next_to(tx3,DOWN).align_to(tx3,LEFT)
        self.add(tx5)
        tx6=Tex(r"$ f(x) = \frac{x}{(1-x)^2} $").next_to(tx2,DOWN).align_to(tx2,LEFT)
        self.add(tx6)
        bg6=BackgroundRectangle(tx6, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
        bg2=BackgroundRectangle(tx2, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
        self.remove(tx2,tx6)
        self.add(bg2,bg6)
        self.add(tx2,tx6)
        self.wait_till("15:40")
        #
        self.clear()
        fs1=FunSequence(scale=3,fun=lambda n: 1/n if n >= 1 else 0) #.next_to(bg3,DOWN).align_to(tx2,LEFT).shift(2*RIGHT)
        self.add(fs1)
        self.add(Text("The Harmonic Series").move_to([0,3,0]))
        tx1=Tex(r"$ f(x) = 0 + \frac{x}{1}  + \frac{x^2}{2} + \frac{x^3}{3} + \ldots + \frac{x^n}{n} +  \ldots $").next_to(fs1,DOWN)
        self.add(tx1)
        tx2=Tex(r"$ F_n = 1/n $").next_to(fs1,LEFT).shift(UP)
        tx2.index=2
        self.add(tx2)
        self.wait_till("16:20")
        tx6=Tex(r"$ f(x) = - log(1-x) $").next_to(tx2,DOWN).align_to(tx2,LEFT)
        self.add(tx6)
        self.wait(10)
        bg6=BackgroundRectangle(tx6, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
        bg2=BackgroundRectangle(tx2, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
        self.remove(tx2,tx6)
        self.add(bg2,bg6)
        self.add(tx2,tx6)
        self.wait_till("16:55")
        #
        self.clear()
        self.add(Text("Some Combinatorics With Generating Functions",font_size=42).move_to([0,3,0]))
        rball=Dot(color=RED,radius=0.2).move_to([-5,2,0])
        rtxt=Text("Red Ball, You Win 1€, chance 20%",font_size=24).next_to(rball,RIGHT)
        self.wait_till("17:00")
        self.add(rball,rtxt)
        gball=Dot(color="#D4AF37",radius=0.2).next_to(rball,DOWN)
        gtxt=Text("Golden Ball, You Win 5€, chance 3%",font_size=24).next_to(gball,RIGHT)
        self.wait(22)
        self.add(gball,gtxt)
        bball=Dot(color=BLACK,radius=0.2).next_to(gball,DOWN)
        btxt=Text("Black Ball, You Loose, chance 77%",font_size=24).next_to(bball,RIGHT)
        self.wait(10)
        self.add(bball,btxt)
        self.wait_till("18:00")
        tx1=Tex(r"$ p(x) = 0.77 \cdot x^0 + 0.20\cdot x^1 + 0.03\cdot x^5  $").next_to(bball,DOWN).align_to(bball,LEFT)
        self.add(tx1)
        self.wait_till("18:50")
        tx2=Tex(r"After 20 rounds: $ f(x) = p(x)^{20} = $",font_size=32).next_to(tx1,DOWN).align_to(bball,LEFT)
        tx3=Tex(r"$  = 0.00536 + 0.0279 \cdot  x + \ldots + 0.1183 \cdot x^4 + 0.103 \cdot x^5 + \ldots + 3.487\cdot 10^{-31} x^{100} $",font_size=32).next_to(tx2,DOWN).align_to(bball,LEFT)
        self.add(tx2,tx3)
        self.wait_till("22:57")
        #
        #
        #
        self.clear()
        headline=Tex(r"Finite Difference Equations: The Fibonacci Sequence").move_to([0, 3, 0])
        self.add(headline)
        self.wait(7)
        fibtex=Tex(r"$ F_{n}=F_{n-1}+F_{n-2} $").move_to([-4,2.0,0])
        fibtex2=Tex(r"\tiny $ F_0=0, F_1 = 1 $").next_to(fibtex,DOWN).align_to(fibtex,LEFT)
        self.add(fibtex,fibtex2)
        self.wait_till("24:04")
        ftex3=Tex(r"$ f(x) = f(x) \cdot x  + f(x) \cdot x^2  + p(x) $").next_to(fibtex2,DOWN).align_to(fibtex,LEFT)
        self.add(ftex3)
        self.wait_till("25:05")
        ftex4=Tex(r"$ f(x)  \cdot ( 1 - x  - x^2 ) = p(x) $").next_to(ftex3,DOWN).align_to(ftex3,LEFT)
        self.add(ftex4)
        self.wait_till("25:51")
        ftex5=Tex(r"$ f(x) = \frac{x}{1- x - x^2 } $").next_to(ftex4,DOWN).align_to(ftex4,LEFT)
        self.add(ftex5)
        self.wait(5) 
        ftxt6=Text("How to get the sequence? - One option long division",font_size=24).next_to(ftex5,DOWN).align_to(ftex5,LEFT)
        self.add(ftxt6)
        self.wait(5)
        ftxt7=Text("Or put it in a computer algebra system, like python sympy",font_size=24).next_to(ftxt6,DOWN).align_to(ftxt6,LEFT)
        self.add(ftxt7)
        self.wait_till("26:00")
        sympyfib= ImageMobject('sympyfib.png').scale(1.5)
        self.play(FadeIn(sympyfib))
        self.add(sympyfib)
        self.wait_till("27:03")
        #
        #
        self.clear()
        headline=Tex(r"Finite Difference Equations: The Fibonacci Sequence").move_to([0, 3, 0])
        self.add(headline)
        self.wait(5)
        ftex1=Tex(r"$ f(x) = \frac{x}{1- x - x^2 } $").move_to([-4,2,0])
        self.add(ftex1)
        self.wait_till("27:44")
        ftxt2=Text("Use Partial Fraction Decomposition",font_size=24).next_to(ftex1,DOWN).align_to(ftex1,LEFT)
        self.add(ftxt2)
        self.wait(5)
        ftex3=Tex(r"$ f(x) = \frac{x}{1- x - x^2 } = \frac{A}{1-\frac{x}{r_1}} + \frac{B}{1-\frac{x}{r_2}} $").next_to(ftxt2,DOWN).align_to(ftex1,LEFT)
        self.add(ftex3)
        self.wait_till("28:55")
        ftex4=Tex(r"$ x^2 + x - 1  = (x-r_1)\cdot (x-r_2) $ ").next_to(ftex3,DOWN).align_to(ftex1,LEFT)
        ftex5=Tex(r"$ r_{1,2}= \frac{-1 \pm \sqrt{5}}{2} $ ").next_to(ftex4,DOWN).align_to(ftex1,LEFT)
        self.add(ftex4,ftex5)
        self.wait_till("29:23")
        ftex6=Tex(r"$ A = -B = \frac{1}{\sqrt{5}} $ ").next_to(ftex5,RIGHT).shift(0.5*RIGHT)
        self.add(ftex6)
        self.wait_till("30:37")
        ftex7=Tex(r"$F_n = \frac{1}{\sqrt{5}} \cdot(  \frac{1}{r_1^n} - \frac{1}{r_2^n} )$ ",font_size=80).next_to(ftex5,DOWN).align_to(ftex1,LEFT)
        ft7bg=BackgroundRectangle(ftex7, color=TEAL_E, fill_opacity=1.0,stroke_color=RED,stroke_width=2, stroke_opacity=1).scale(1.1)
        self.add(ft7bg,ftex7)
        self.wait_till("31:21")
        #
        #
        #
        self.clear()
        headline=Tex(r"Including values at negative position").move_to([0, 3, 0])
        self.add(headline)
        self.wait(10)
        fs1=FunSequence(start=-4,scale=1,fun=lambda n: 1.5 if n==-2 else 1 if n > -3 else 0 ).move_to([4,1,0])
        self.add(fs1)
        self.wait_till("31:46")
        ftex1=Tex(r"$ f(x) = 1.5x^{-2} + x^{-1} + \frac{1}{1-x} $").next_to(fs1,DOWN).shift(LEFT*3)
        self.add(ftex1)
        self.wait_till("32:04")
        #
        self.clear()
        headline=Tex(r"But infinitly many negative positions are a bit of a problem").move_to([0, 3, 0])
        self.add(headline)
        self.wait(9)
        fs1=FunSequence(start=-11,stop=11,scale=1,fun=lambda n: 1 if n < 0 else 0 ).move_to([0,1,0])
        self.add(fs1)
        self.wait_till("32:31")
        ftex1=Tex(r"$ f(x) = x^{-1} + x^{-2} + x^{-3} + \ldots $").next_to(fs1,DOWN).shift(LEFT)
        self.add(ftex1)
        self.wait(5)
        ftex2=Tex(r"$ f(x) = \frac{-1}{1-x} $").next_to(ftex1,DOWN).align_to(ftex1,LEFT)
        self.add(ftex2)
        fs2=FunSequence(start=-11,stop=11,scale=1,fun=lambda n: -1 if n >=0  else 0 ).next_to(fs1,DOWN).shift(2*DOWN)
        self.add(fs2)
        self.wait_till("33:30")
        self.add(Tex("Region of Convergence (RoC) $ x>1 $",font_size=36).next_to(fs1,UP).shift(1.5*RIGHT))
        self.wait(2)
        self.add(Tex("Region of Convergence (RoC) $ x<1 $",font_size=36).next_to(fs2,UP).shift(1.5*RIGHT))
        self.wait_till("34:30")
        #
        #
        #
      self.clear()
      headline=Tex(r"Infinite Grid of Resistors in 1D").move_to([0, 3.4, 0])
      self.add(headline)
      self.wait(10)
      p0=None
      for i  in range(8):
        R1=Resistor(r"$ 1\Omega $").scale(2).rotate_about_origin(90*DEGREES)
        R1.move_to([-6+i*2,2.5,0])
        self.add(R1)
        self.add(Tex(r"\tiny $ U_{"+str(i)+"}$").next_to(R1).shift(UP*0.2+LEFT*0.25))
        if i==0:
          p0=R1.dots[0]
      pos0=p0.get_top()    
      self.add(Line(p0.get_top(),p0.get_center()+3*UP,stroke_width=2))  
      self.wait_till("35:10")
      ftex1=Tex(r"$ (U_{n-1}- U_n) - ( U_n - U_{n+1} ) = U_{n-1} - 2 \cdot U_{n} + U_{n+1} = 1\Omega \cdot I_n $",font_size=40).move_to([-1,1.5,0])
      self.add(ftex1)
      I1=Arrow(pos0+1.1*UP,pos0+1.4*UP,stroke_width=3.0,stroke_color=RED,max_tip_length_to_length_ratio=1.0)
      self.add(I1)
      self.wait_till("37:00")
      ftex2=Tex(r"Generating Function for  $ U_n $ is $ u(x) $",font_size=40).next_to(ftex1,DOWN).align_to(ftex1,LEFT)
      self.add(ftex2)
      self.wait(5)
      ftex3=Tex(r"Voltage over a resistor is the difference in $ U_{n-1} - U_{n} $ \\ this givs $ u_R = u(x) \cdot (1-x) $",font_size=40).next_to(ftex2,DOWN).align_to(ftex1,LEFT)
      self.add(ftex3)
      self.wait_till("38:25")
      ftex4=Tex(r"The difference in I between neighboring resistors is $ \frac{1}{1\Omega} \cdot (U_{R_n} - U_{R_{n-1}} ) $ \\ this givs $ i(x) = u(x) \cdot (1-x) \cdot (1-x) $",font_size=40).next_to(ftex3,DOWN).align_to(ftex1,LEFT)
      self.add(ftex4)
      self.wait(5)
      ftex5=Tex(r"$ u(x) = \frac{1}{(1-x)^2} $ ",font_size=56).next_to(ftex4,DOWN).align_to(ftex1,LEFT)
      self.add(ftex5)
      self.wait_till("39:40")
      ftex5=Tex(r"$ = 1 + 2x + 3x^2 + 4x^3 + 5x^4 + \ldots $ ",font_size=56).next_to(ftex5,RIGHT) # .align_to(ftex1,LEFT)
      self.add(ftex5)
      self.wait_till("40:20")
      #
      self.clear()
      fs1=FunSequence(xscale=0.4,stop=24,scale=0.5,fun=lambda n: np.round(2*np.sin(n*np.pi/2)+np.cos(n*np.pi/2)) if n >= 0 else 0)
      self.add(fs1)
      self.add(Text("Complex Roots cause Oscilations").move_to([0,3,0]))
      self.wait_till("40:30")
      tx1=Tex(r"$ f(x) = \frac{1+2x}{ 1+x^2 } $",font_size=86).next_to(fs1,LEFT).shift(UP+2*RIGHT)
      self.add(tx1)
      self.wait_till("41:20")
      tx2=Tex(r"$ r_{1,2} = \pm i $",font_size=96).move_to([0,-2,0])
      self.add(tx2)
      self.wait_till("42:32")
      #
      #
      #
      self.clear()
      headline=Tex(r"Z-Transform").move_to([0, 3.4, 0])
      self.add(headline)
      self.wait(28)
      tx1=Tex(r"$ f(x) \Rightarrow f(z^{-1}) $").move_to([-3,2.5,0])
      self.add(tx1)
      self.wait_till("43:25")
      tx2=Tex(r"$f(z)=\frac{1}{1-z^{-1}}=\frac{z}{z-1} $").next_to(tx1,DOWN).align_to(tx1,LEFT)
      tx3=Tex(r"$f(z)= 1 + z^{-1} + z^{-2} +z^{-3} +z^{-4} + \ldots  $").next_to(tx2,DOWN).align_to(tx2,LEFT)
      fs1=FunSequence(scale=1,fun=lambda n: 1 if n >= 0 else 0).next_to(tx3,DOWN).shift(3*RIGHT+DOWN)
      self.add(tx2)
      self.wait(2)
      self.add(tx3)
      self.wait(2)
      self.add(fs1)
      self.wait_till("43:48")
      #
      self.clear()
      headline=Tex(r"Z-Transform has its origin in the Laplace Transform").move_to([0, 3.4, 0])
      self.add(headline)
      self.wait(5)
      tx1=Tex(r"$ \mathcal{L} \{ \delta (t-T) \} = e^{-sT} $").move_to([-3,2.5,0])
      self.add(tx1)
      self.wait_till("45:15")
      tx2=Tex(r"$ \mathcal{L} \{ \sum\limits_n \delta (t-nT) \} = \frac{1}{1-e^{-sT}} $ \\ replace $\underrightarrow{e^{sT}=z}$ gives $ \frac{1}{1-z^{-1}} $").next_to(tx1,DOWN).align_to(tx1,LEFT)
      self.add(tx2)
      self.wait(5)
      tx3=Tex(r"When transforming back: \\ multiply with $ \frac{1}{s}(1-e^{-sT}) $ to get steps instead of spikes").next_to(tx2,DOWN).align_to(tx1,LEFT)
      self.add(tx3)
      self.wait_till("46:58")
      #
      #
      #
      self.clear()
      headline=Tex(r"Digital Filters").move_to([0, 3.4, 0])
      self.add(headline)
      self.wait(5)
      tx1=Tex(r"$ Y_n = \sum\limits_{k=0}^M X_{n-k} B_k - \sum\limits_{i=1}^N Y_{n-i} A_i $   with $A_0=1$ ").move_to([-2,2.5,0])
      self.add(tx1)
      self.wait_till("49:52")
      tx2=Tex(r"$ y(z)\cdot a(z) = x(z) \cdot b(z)  $").next_to(tx1,DOWN).align_to(tx1,LEFT)
      self.add(tx2)
      self.wait_till("50:23")
      tx3=Tex(r"$ h(z)=\frac{y(z)}{x(z)} = \frac{b(z)}{a(z)} $").next_to(tx2,DOWN).align_to(tx1,LEFT)
      self.add(tx3)
      tx4=Tex(r"$ y(z)=x(z) \cdot h(z) $").next_to(tx3,DOWN).align_to(tx1,LEFT)
      self.add(tx4)
      self.wait_till("51:38")
      #
      #
      self.clear()
      myTemplate = TexTemplate()
      myTemplate.add_to_preamble(r"\usepackage{trfsigns}")
      headline=Tex(r"Back to the Grid: 2D Generating Functions").move_to([0, 3.4, 0])
      self.add(headline)
      self.wait(5)
      tx1=Tex(r"$  F_{m,n} \laplace f(x,y) $ ",tex_template=myTemplate).move_to([-4.8,2,0])
      self.add(tx1)
      self.wait(5)
      tx2=Tex(r"$  f(x,y)= F_{0,0} + F_{1,0} x + F_{0,1} y + F_{1,1} x y + F_{2,0} x^2 + F_{2,1} x^2 y + \ldots ..  $ \\ $  \ldots + F_{m,n}x^m y^n + \ldots  $ ").next_to(tx1,DOWN).align_to(tx1,LEFT)
      self.add(tx2)
      self.wait_till("54:45")

      
      