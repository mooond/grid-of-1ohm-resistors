from manim import *
import numpy as np
#from manim.imports import * 
from manim_physics import *
import lcapy


class MultiMeter(VGroup):
   def __init__(self,pos=None,red=None,black=None,rtred=35,rtblack=-35,disp=None):
        super().__init__()
        self.pos=pos
        mm = SVGMobject("multimeter.svg",stroke_width=0.0).scale(1.5)
        rt=  SVGMobject("redtip.svg",stroke_width=0.0)
        bt=  SVGMobject("blacktip.svg",stroke_width=0.0)
        self.mm=mm
        if pos:
          mm.move_to(pos)
        self.add(mm)
        if red:
          rt.move_to(red)
          rt.shift(rt.height*DOWN*0.5)
          rtarget=rt.get_top() + [rt.height*np.sin(rtred*DEGREES),-rt.height*np.cos(rtred*DEGREES),0]
          rt.rotate(rtred*DEGREES,about_point=rt.get_top())
          #t1=red.get_top()
          #delta=rt.get_top()-t1
          #rt.move_to(UP*delta)
          self.add(rt)
          rstart=mm.get_bottom()+RIGHT*0.6+UP*0.57
          bezier = CubicBezier(rstart, rstart + 1 * RIGHT, rtarget + [rt.height*np.sin(rtred*DEGREES),-rt.height*np.cos(rtred*DEGREES),0] , rtarget, stroke_width=6)
          bezier.color="#A00000"
          self.add(bezier)
          
        if black:
          bt.move_to(black)
          bt.shift(bt.height*DOWN*0.5)
          btarget=bt.get_top() + [bt.height*np.sin(rtblack*DEGREES),-bt.height*np.cos(rtblack*DEGREES),0]
          bt.rotate(rtblack*DEGREES,about_point=bt.get_top())
          #t2=black.get_top()
          #delta=bt.get_top()-t2
          #bt.move_to(UP*delta)  
          self.add(bt)
          bstart=mm.get_bottom()+RIGHT*0.6+UP*0.32
          bezier = CubicBezier(bstart, bstart - 1 * UP, btarget + [bt.height*np.sin(rtblack*DEGREES),-bt.height*np.cos(rtblack*DEGREES),0] , btarget, stroke_width=6)
          bezier.color="#000000"
          self.add(bezier)
        if disp:
          disptxt=Text(disp,font="DSEG14 Classic",font_size=20,color=BLACK)
          disptxt.move_to(mm.get_top() + DOWN*0.4+LEFT*0.15)
          self.disptxt=disptxt
          self.add(disptxt)
          
   def update_text(self,newtext):
      disptxt=self.disptxt
      newdisp=Text(newtext,font="DSEG14 Classic",font_size=20,color=BLACK)
      newdisp.move_to(self.mm.get_top() + DOWN*0.4+LEFT*0.15)
      return Transform(disptxt,newdisp)

class Resistor(VGroup):
   def __init__(self,txt=None):
        super().__init__()
        self.txt=txt
        self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        self.body=Rectangle(height=0.5, width=0.15, stroke_width=2)
        self.body.shift(2*UP/4)
        for d in self.dots:
          self.add(d)
        self.add(self.body)
        if self.txt:
          self.tex = Tex(txt, font_size=12)
          self.tex.shift(UP/2+RIGHT/5)
          self.add(self.tex)  
        self.add(Line(self.dots[0],UP/4,stroke_width=2))
        self.add(Line(UP*3/4,self.dots[1],stroke_width=2))

class Battery(VGroup):
   def __init__(self,txt=None):
        super().__init__()
        self.txt=txt
        self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        for d in self.dots:
          self.add(d)
        self.add(Line(self.dots[0],UP*2/5,stroke_width=2))
        self.add(Line(UP*3/5,self.dots[1],stroke_width=2))
        lplus=Line(UP*2/5+LEFT*0.2,UP*2/5-LEFT*0.2,stroke_width=2)
        self.add(lplus)
        self.add(Line(UP*3/5+LEFT*0.3,UP*3/5-LEFT*0.3,stroke_width=2))
        self.add(Text("+",font_size=18).move_to(UP*3.3/5+LEFT*0.2))
        self.add(Text("-",font_size=18).move_to(UP*1.7/5+LEFT*0.2))
        if self.txt:
          self.tex = Tex(txt, font_size=24)
          self.tex.shift(UP/2) #
          self.tex.next_to(lplus,LEFT)
          self.add(self.tex)  


class CurrentSource(VGroup):
   def __init__(self,txt=None):
        super().__init__()
        self.txt=txt
        self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        for d in self.dots:
          self.add(d)
        circ=Circle(radius=1.0/4.0,color=WHITE,stroke_width=2).shift(UP*1.0/2.0)
        self.add(circ)
        self.add(Line(self.dots[0],UP*1/4,stroke_width=2))
        self.add(Line(UP*3/4,self.dots[1],stroke_width=2))
        self.add(Arrow( [0,1.3/4.0,0], [0,2.7/4,0] ))
        if self.txt:
          self.tex = Tex(txt, font_size=24)
          self.tex.shift(UP/2).next_to(circ,LEFT)
          self.add(self.tex)  


class Short(VGroup):
   def __init__(self,replace=None):
        super().__init__()
        if replace:
          self.dots=replace.dots.copy()
        else:
          self.dots=[Dot(radius=0.03),Dot(UP,radius=0.03)]
        for d in self.dots:
          self.add(d)
        self.add(Line(self.dots[0],self.dots[1],stroke_width=2))


class scene1s2(MovingCameraScene):
    def wait_till(self,n):
      ns=n
      ts=self.timestamp
      if isinstance(n,str):
        fval=[1,60,60]
        nfields=n.split(":")
        n=sum([fval*int(nf) for fval,nf in zip(fval,nfields[::-1])])
        print("waitting till",n,ns)
      tdelta=n-self.renderer.time-ts
      if tdelta < 0:
        print("============= BEHIND SCHEDULE ==============")
      elif tdelta > 0:  
        self.wait(tdelta)
        
    def construct(self):
      self.timestamp=0
      self.camera.background_color = '#403800'
      # r1 = drawgrid(10)
      # r1.draw(style='european',scale=3,filename="tmp0s1.svg",color='white')
      R1=Resistor(r"$R_1= 150 \Omega $").scale(4)
      R1.tex.shift(1*RIGHT)
      self.add(Text("Ohm's Law").move_to(3.3*UP))
      self.add(R1)
      self.wait_till("00:30")
      mume=MultiMeter(Point([-4,-2,0]),R1.dots[0],R1.dots[1],disp="150Ω")
      self.add(mume)
      self.wait_till("00:55")
      self.remove(mume)
      U1=ArcBetweenPoints(R1.dots[1].get_top(),R1.dots[0].get_top(),color=BLUE)
      U1.add_tip()
      self.play(FadeIn(U1))
      self.add(U1)
      self.add(Tex(r"$ U_1= 30V $",color=BLUE).move_to(U1.get_center()+0.6*UP+1.5*LEFT))
      self.wait(5)
      ipos=-0.2*(R1.dots[1].get_top()-R1.dots[0].get_top())+R1.dots[1].get_top()
      I1=Arrow(ipos+0.1*UP,ipos-0.1*UP,stroke_width=3.0,stroke_color=RED,max_tip_length_to_length_ratio=2.0)
      self.add(I1)
      itex=Tex(r"\small $I_1=\frac{30V}{150\Omega}=0.2A$",color=RED).move_to(ipos+2*RIGHT)
      self.play(FadeIn(itex))
      self.add(itex)
      self.wait_till("01:12")
      cnt=0
      for rule in [r"$ I=\frac{U}{R} $",
                   r"$ R=\frac{U}{I} $",
                   r"$ U= I \cdot R $"]:
        texrule=Tex(r"\Large " + rule).move_to(2*UP+1.5*cnt*DOWN+4.6*LEFT)
        cnt += 1
        self.play(FadeIn(texrule))
        self.add(texrule)
        self.wait(3) 
      self.wait_till("01:33")
      self.clear()
      R1=Resistor(r"$R_1= 100 \Omega $").scale(3).shift(1.5*UP)
      R1.tex.shift(1*RIGHT)
      R2=Resistor(r"$R_2= 200 \Omega $").scale(3).shift(1.5*DOWN)
      R2.tex.shift(1*RIGHT)
      self.add(R1,R2)
      self.wait_till("01:50")
      ipos=-0.2*(R1.dots[1].get_top()-R1.dots[0].get_top())+R1.dots[1].get_top()
      I1=Arrow(ipos+0.1*UP,ipos-0.1*UP,stroke_width=3.0,stroke_color=RED,max_tip_length_to_length_ratio=1.0)
      self.add(I1)
      itex=Tex(r"\small $ I = I_1  = I_2 $",color=RED).move_to(ipos+2*RIGHT)
      self.play(FadeIn(itex))
      self.add(itex)
      self.wait_till("02:00")
      self.add(Tex(r"\tiny $ U_1 = I \cdot R_1$",color=BLUE).move_to([4,1,0]))
      self.add(Tex(r"\tiny $ U_2 = I \cdot R_2$",color=BLUE).move_to([4,-2,0]))
      U1=ArcBetweenPoints(R1.dots[1].get_top(),R2.dots[0].get_top(),color=BLUE)
      U1.add_tip()
      self.play(FadeIn(U1))
      self.add(U1)
      self.add(Tex(r"\small $ U = U_1 + U_2$",color=BLUE).move_to(U1.get_center()+0.6*UP+3*LEFT))
      self.wait_till("02:16")
      self.add(Tex(r"$ R=R_1+R_2 = \frac{U}{I} = \frac{U_1+U_2}{I} = \frac{I \cdot R_1+ I \cdot R_2}{I} $").shift(3*DOWN))
      self.wait_till("02:32")
      self.clear()
      R1=Resistor(r"$R_1$").scale(3).shift(1.5*LEFT)
      #R1.tex.shift(0.5*RIGHT)
      R2=Resistor(r"$R_2$").scale(3).shift(1.5*RIGHT)
      #R2.tex.shift(*RIGHT)
      self.add(R1,R2)
      self.add(Line(R1.dots[0],R2.dots[0]))
      self.add(Line(R1.dots[1],R2.dots[1]))
      mup=  (R1.dots[1].get_center() + R2.dots[1].get_center())/2.0
      mdown=(R1.dots[0].get_center() + R2.dots[0].get_center())/2.0
      self.add(Line(mup,mup+1*UP))
      self.add(Dot(mup),Dot(mup+1*UP),Dot(mdown),Dot(mdown+1*DOWN))
      self.add(Line(mdown,mdown+1*DOWN))
      self.wait_till("02:37")
      ipos=mup+0.5*UP
      I1=Arrow(ipos+0.1*UP,ipos-0.1*UP,stroke_width=3.0,stroke_color=RED,max_tip_length_to_length_ratio=1.0)
      self.add(I1)
      itex=Tex(r"\small $ I = I_1 + I_2 $",color=RED).move_to(ipos+2*RIGHT)
      self.play(FadeIn(itex))
      self.add(itex)
      self.wait_till("02:49")
      U1=ArcBetweenPoints(R1.dots[1].get_top(),R1.dots[0].get_top(),color=BLUE)
      U1.add_tip()
      self.play(FadeIn(U1))
      self.add(U1)
      self.add(Tex(r"\small $ U = U_1 = U_2$",color=BLUE).move_to(U1.get_center()+0.6*UP+3*LEFT))
      self.wait_till("03:02")
      self.add(Tex(r"\Large $ \frac{1}{R}=\frac{1}{R_1}+\frac{1}{R_2} $").shift(3*RIGHT+2*DOWN))
      self.wait_till("03:21")
      self.clear()
      net = lcapy.Circuit()
      net.add("""
      R1 A B ; down=1.7, 
      R2 B C ; down=1.7
      W 4_0 C; right
      W 1_0 A; right, i= ,
      P 4_0 1_0; up, v= ,
      R3 B 5_0; right=1.7
      R4 5_0 6_0; down=1.7
      W 6_0 C; left
      """)
      net.draw(style='european',scale=0.3,filename="tmp1s1b.svg")
      rnet=SVGMobject("tmp1s1b.svg",stroke_width=2.0,stroke_color=WHITE).scale(3).shift(2*RIGHT)
      self.add(rnet)
      cnt=0
      self.wait_till("03:47")
      for rule in [r"$ R_{34}=R_3 + R_4 $",
                   r"$ R_{234}=\frac{1}{\frac{1}{R_2}+\frac{1}{R_3+R_4}} $",
                   r"$ R_{total}= R_1 + R_{234} $"]:
        texrule=Tex(r"\large " + rule).move_to(3*UP+1.5*cnt*DOWN+4.6*LEFT)
        cnt += 1
        self.play(FadeIn(texrule))
        self.add(texrule)
        self.wait(5) 
      net2 = lcapy.Circuit()
      net2.add("""
      R1 A B ; down=1.7, 
      R2 B C ; down=1.7
      W 4_0 C; right
      W 1_0 A; right, i= ,
      P 4_0 1_0; up, v= ,
      W B 5_0; right=1.7
      R34 5_0 6_0; down=1.7
      W 6_0 C; left
      """)
      net2.draw(style='european',scale=0.3,filename="tmp1s1c.svg")
      rnet2=SVGMobject("tmp1s1c.svg",stroke_width=2.0,stroke_color=WHITE).scale(3).shift(2*RIGHT)
      self.play(Transform(rnet,rnet2),run_time=0.5)
      self.wait_till("04:02")
      net3 = lcapy.Circuit()
      net3.add("""
      R1 A B ; down=1.7, 
      R234 B C ; down=1.7
      W 4_0 C; right
      W 1_0 A; right, i= ,
      """)
      net3.draw(style='european',scale=0.3,filename="tmp1s1d.svg")
      rnet3=SVGMobject("tmp1s1d.svg",stroke_width=2.0,stroke_color=WHITE).scale(3).shift(2*RIGHT)
      self.wait_till("04:15")
      self.play(Transform(rnet,rnet3),run_time=0.5)
      self.wait_till("04:30")
      self.clear()
      net = lcapy.Circuit()
      net.add("""
      R1 A B ; down=1.7, 
      R2 B C ; down=1.7
      W 4_0 C; right
      W 1_0 A; right, 
      P 4_0 1_0; up
      R5 B E; right=1.7
      R4 E F; down=1.7
      W F C; left
      W D A; left=1.7
      R3 D E; down=1.7 
      ;style=european, bipole label style={color=blue}
      """)
      net.draw(style='european',scale=0.3,filename="tmp1s1w.svg")
      rnet=SVGMobject("tmp1s1w.svg",stroke_width=2.0,stroke_color=WHITE).scale(2.5).shift(3.7*RIGHT)
      self.add(rnet)
      self.add(Text("It is not always that easy").move_to(3.3*UP))
      self.wait_till("4:47")
      cnt=0
      for rule in [r"If possible: Use Symetry",
                   r"Use Star-Triangle Substitution",
                   r"Use System of Equation (Kirchhoff's Laws)"]:
        texrule=Tex(r"\small " + rule).move_to(2*UP+1.5*cnt*DOWN+2.5*LEFT)
        cnt += 1
        self.play(FadeIn(texrule))
        self.add(texrule)
        self.wait(20) 
      
      self.wait_till("07:03")
      self.clear()
      #
      #
      self.add(Tex("A Much Simpler Infinite Network").move_to([0,3.5,0]))
      net = lcapy.Circuit()
      n=9
      for i in range(n):
        p1=i*2+1
        p2=i*2+2
        net.add(f"""
          R{p1} {p1} {p1+2}   ;right=1.5, l=1\Omega
          W {p2} {p2+2}       ;right=1.5 
          R{p2} {p1+2} {p2+2} ; down=1.5, l=1\Omega
          """)
      net.add(f"""
        W {n*2+1} {n*2+3}_0  ;right=0.5, l=...  
        W {n*2+2} {n*2+4}_0  ;right=0.5, l=...
        """)
      net.draw(style='european',scale=0.3,filename="tmp1s1g.svg")
      rnet=SVGMobject("tmp1s1g.svg",stroke_width=2.0,stroke_color=WHITE).scale(1).shift(2*RIGHT+2*DOWN)
      self.add(rnet)
      net2 = lcapy.Circuit()
      n=1
      for i in range(n):
        p1=i*2+1
        p2=i*2+2
        net2.add(f"""
          R{p1} {p1} {p1+2}   ;right=1.5, l=1\Omega
          W {p2} {p2+2}       ;right=1.5 
          R{p2} {p1+2} {p2+2} ; down=1.5, l=1\Omega
          """)
      net2.add(f"""
        W {n*2+1} {n*2+3}_0  ;right=1   
        W {n*2+2} {n*2+4}_0  ;right=1
        R {n*2+3}_0 {n*2+4}_0 ; down=1.5
      """)
      net2.draw(style='european',scale=0.3,filename="tmp1s1g2.svg")
      rnet2=SVGMobject("tmp1s1g2.svg",stroke_width=2.0,stroke_color=WHITE).scale(1).shift(2*RIGHT+2*DOWN)
      rnet2.align_to(rnet,LEFT)
      self.wait_till("07:34")
      self.add(Text("R").shift(5.3*LEFT+2*DOWN))
      self.add(Line([0,1.3,0],[0,-1.3,0],color=RED).shift(3.7*LEFT+2*DOWN))
      self.wait_till("07:40")
      self.play(Transform(rnet,rnet2),run_time=0.5)
      self.wait(5)
      pa=Point([-1.5,3,0])
      tex2=[r"$ R=1+\frac{1}{ \frac{1}{1}+\frac{1}{R} } $",
            r"$ R=1+\frac{R}{R+1} $",
            r"$ R^2-R-1 =0 $",
            r"$ R = \frac{1}{2}  \pm \frac{\sqrt{5}}{2} $",
            r"$ R= \frac{1}{2} + \frac{\sqrt{5}}{2} = 1.618033\ldots $",
            r"$ R= $  ``The Golden Ratio'' "] 
      for t in tex2:
            print("converting",t)
            t1=Tex(t).next_to(pa,DOWN).align_to(pa,LEFT)
            pa=t1
            self.play(Write(t1))
            self.add(t1)
            self.wait(15)

      ##
      #
      self.wait_till("09:43")
      self.clear()
      self.add(Text("The Superposition Principle").move_to(3.3*UP))
      rect=Rectangle(width=3.0, height=6.0).shift(3*RIGHT)
      rect.set_fill(BLACK, opacity=0.5)
      self.add(rect)
      self.add(Text("Black Box").scale(0.8).move_to(rect.get_center()))
      self.wait_till("10:07")
      R1=Resistor(r"$R_{417}$").scale(2).shift(5.5*RIGHT)
      self.add(Line(R1.dots[0],R1.dots[0].get_center()+0.9*LEFT))
      self.add(Line(R1.dots[1],R1.dots[1].get_center()+0.9*LEFT))
      self.add(R1)
      self.wait_till("10:27")
      mume=MultiMeter(Point([4,-2,0]),R1.dots[1],R1.dots[0],disp="---")
      self.add(mume)
      inputs=[]
      for ind1 in [-2,0,2]:
        pnt1=[1,ind1,0]
        pnt2=[1,ind1+0.5,0]
        inputs.append( np.array([pnt1,pnt2]) )
        self.add(Dot(pnt1),Dot(pnt2))
        self.add(Line(pnt1,pnt1+0.5*RIGHT))
        self.add(Line(pnt2,pnt2+0.5*RIGHT))
      bat1=Battery("$U_1=5V$").shift(1.5*UP+LEFT)
      bat2=Battery("$U_2=8V$").shift(0.5*DOWN+LEFT)
      short1=Short(bat1)
      short2=Short(bat2)
      src3=CurrentSource("$I_3=0.5A$").shift(2.5*DOWN+LEFT)
      self.add(bat1,bat2,src3)
      sources=[bat1,bat2,src3]
      self.add(Tex(r"\small $ f(U_1 , U_2 , I_3) = U_{out} $").shift(2.5*UP).to_edge(LEFT))
      #connect
      self.wait_till("11:00")
      lines=[]
      for src,inp in zip(sources,inputs[::-1]):
        line1=Line(src.dots[0],inp[0])
        line2=Line(src.dots[1],inp[1])
        lines.append( (line1,line2) )
        self.add(line1,line2)
      self.play(mume.update_text("21V"),runt_time=2.0)
      self.add(Tex(r"\tiny $ f(5V,8A,0.5A) = 21V $").shift(1.6*UP).to_edge(LEFT))
      self.wait_till("11:26")
      # disconnect
      bat1.save_state()
      bat2.save_state()
      self.remove(src3)
      self.play(Transform(bat1,short1),Transform(bat2,short2),mume.update_text("0V"),run_time=2)
      self.add(Tex(r"\tiny $ f(0V,0V,0A) = 0V $").shift(1.1*UP).to_edge(LEFT))
      self.wait_till("11:59")
      #
      self.play(Restore(bat1),mume.update_text("2V"),run_time=2)
      self.add(Tex(r"\tiny $ f(5V,0V,0A) = 2V $").shift(0.6*UP).to_edge(LEFT))
      self.wait_till("12:33")
      #
      self.play(Restore(bat2),Transform(bat1,short1),mume.update_text("6V"),run_time=2)
      self.add(Tex(r"\tiny $ f(0V,8V,0A) = 6V $").shift(0.1*UP).to_edge(LEFT))
      #
      self.wait_till("12:55")
      self.play(Transform(bat1,short1),Transform(bat2,short2),FadeIn(src3),mume.update_text("13V"),run_time=2)
      self.add(src3)
      self.add(Tex(r"\tiny $ f(0V,0V,0.5A) = 13V $").shift(-0.4*UP).to_edge(LEFT))
      self.wait_till("13:25")
      self.add(Tex(r"\tiny $ f(5,0,0)+ f(0,8,0)+ f(0,0,0.5)  = $ \\ $  = 2+6+13 = 21V $").shift(-1*UP).to_edge(LEFT))
      self.wait_till("14:04")
      src3b=CurrentSource("$I_3=1A$").shift(2.5*DOWN+LEFT)
      self.play(Transform(src3,src3b),mume.update_text("26V"),run_time=2)
      self.add(Tex(r"\tiny $ f(0V,0V,1A) = 26V $").shift(-1.6*UP).to_edge(LEFT))
      self.wait_till("14:22")
      self.clear()
      #
      #
      imax=7
      jmax=7
      #self.add(Dot([0,0,0],color=RED,radius=0.1),Dot([1,0,0],color=BLUE,radius=0.1),Dot([0,1,0],color=GREEN,radius=0.1))
      for d in range(2):
        for i in range(imax+1):
          for j in range(jmax+1):
            R1=Resistor()
            if d==1:
              #R1.rotate(90*DEGREES,about_point=[0,0,0])
              R1.rotate_about_origin(90*DEGREES).scale(2).shift(LEFT*0.5)
            else:
              R1.scale(2).shift(UP*0.5)  
            R1.shift(2*UP*(i-imax//2) + 2*RIGHT*(j-jmax//2))
            if not (j==jmax and d==0) and not (i==imax and d==1): 
              self.add(R1)
          #self.wait(0.1)
      self.wait_till("14:32")    
      self.add(Text("A",font_size=12).move_to([0.1,0.1,0]))
      self.add(Text("B",font_size=12).move_to([0.1,2.1,0]))
      iarrow=Arrow([-1.1,-1.1,0],[0.1,0.1,0],stroke_width=2.0,stroke_color=RED,max_tip_length_to_length_ratio=0.2)
      self.add(iarrow)
      itex=Tex(r"\tiny $ I_1 = 1A $",color=RED).move_to([-1.2,-1.2,0])
      self.camera.frame.save_state()
      self.play(self.camera.frame.animate.set(width=8),run_time=2.0)
      self.wait_till("15:48")
      ilist=[]
      for d in range(4):
         t1=Text("0.25A",font_size=8,color=RED).shift(0.4*UP+0.15*RIGHT)
         t1.rotate_about_origin(90*DEGREES*d)
         a1=Arrow([0,0.3,0],[0.0,0.4,0],stroke_width=2.0,stroke_color=RED,max_tip_length_to_length_ratio=1.2)
         a1.rotate_about_origin(90*DEGREES*d)
         self.add(a1,t1)
         ilist.append(a1)
         ilist.append(t1)
      self.add(itex)
      self.wait_till("16:15")
      U1=ArcBetweenPoints([0,0,0],[0,2,0],color=BLUE,stroke_width=2.0)
      U1.add_tip()
      self.add(U1)
      u1tex=Tex(r"$U_1=0.25V$",font_size=12,color=BLUE).shift(1*UP+0.9*RIGHT)
      self.add(u1tex)
      self.wait_till("16:47")
      self.remove(*ilist)
      iarrow2=Arrow([-1.1,2-1.1,0],[0.1,2.1,0],stroke_width=2.0,stroke_color=RED,max_tip_length_to_length_ratio=0.2)
      self.add(iarrow2)
      itex2=Tex(r"\tiny $ I_2 = -1A $",color=RED).move_to([-1.2,2-1.2,0])
      self.add(itex2)
      self.wait_till("16:50")
      self.remove(u1tex)
      u1tex=Tex(r"$U_1=0.25V$ \\ $U_2=-(-0.25V)$ \\ $ U=0.25V+0.25V=0.5V$",font_size=14,color=BLUE).shift(1*UP+0.9*RIGHT)
      self.add(u1tex)
      self.wait_till("18:15")
      self.remove(itex,itex2,iarrow,iarrow2)
      cs=CurrentSource().rotate_about_origin(180*DEGREES*d).shift(LEFT+UP*1.5)
      self.add(cs)
      self.add(Line(cs.dots[0],[0,2,0]))
      self.add(Line(cs.dots[1],[0,0,0]))
      self.add(Arrow(cs.dots[1].get_center()*0.9,cs.dots[1].get_center()*0.6,stroke_width=2.0,stroke_color=RED,max_tip_length_to_length_ratio=0.4))
      self.add(Tex(r"$ I=1A $",font_size=14,color=RED).move_to([-0.5,0.6,0]))
      self.add(Tex(r" $ R=\frac{U}{I}=\frac{0.5V}{1A}=0.5 \Omega $",font_size=14).move_to([-1.0,-0.6,0]))
      self.play(self.camera.frame.animate.set(width=5),run_time=5.0)
      self.wait_till("19:18")
      self.play(Restore(self.camera.frame),run_time=1.0)
      self.clear()
      imax=11
      jmax=13
      for d in range(2):
        for i in range(imax+1):
          for j in range(jmax+1):
            R1=Resistor(r"$1\Omega$")
            if d==1:
              #R1.rotate(90*DEGREES,about_point=[0,0,0])
              R1.rotate_about_origin(90*DEGREES) #.shift(LEFT*0.5)
              #else:
              #R1.shift(UP*0.5)  
            R1.shift(1*UP*(i-imax//2) + 1*RIGHT*(j-jmax//2))
            if not (j==jmax and d==0) and not (i==imax and d==1): 
              self.add(R1)
          #self.wait(0.1)
      self.wait_till("19:20")
      mume=MultiMeter(Point([-1.6,-0.8,0]),[0,1,0],[0,0,0],disp="0.5Ω")
      self.play(FadeIn(mume))
      self.add(mume)
      self.wait_till("19:41")
      self.play(self.camera.frame.animate.move_to(Point([0.0,0.5,0])).scale(0.4),run_time=5.0)
      self.wait_till("20:07")   
      
      
      