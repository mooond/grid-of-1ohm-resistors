# Motivation

I will upload more code here soon...

A while ago whily I helped my son to study for an electrial engeneering test and explained to hime how to calculate the electrical resistance in some network of resistors.

At some point I rememberd what I heard some 40 years ago when I was studing this myself: That if you build an infinite grid of electrical resistors that the resulting resistance between two digaonal points is 

$$ \frac{2}{\pi} \Omega $$ 

So I started to google around on how this could be calculated. It turns out that with a simple trick it is easy to calculate the resistance between 2 nearby points. The resistance is just 

$$ \frac{1}{2} \Omega $$

But that the digaonal case is much harder and there where no real good explainations online on how to calculate it. So this is here to fill in the gap and I will try to provide an understandable explaination for the complicated calculation. If you do not know much about electrical resistance or advanced mathematics you should still be able to follow it. Even if you are not so interested in the electrical aspects: There are plenty of pure math aspects and many of the tools here have some use in many other fields as well.

E.g. The same equations are used to approximate many other physical problems, e.g. heat equations or electrostratics. Also the same equations are used in computer graphics for dedection of edges in pictures. So it should be useful.

Along the way I hope to be able to provide you with everything you will need. I want to provide a video with some nice animation and a python-jupyter notebook with some python code. 

- **Chapter I - Electrical Resistance and Simple Electrical Networks**
     - Basics of Electricity: Voltage, Potential, Curret
     - Resistors: Ohm's Law
     - Connecting Multiple Reistors together
     - Kirchhoff's Law
     - More Complicated Resistor Networks
     - The Superposition Principle
     - An easier Infinite Network of Resitors
     - The Infinite Grid - Our First Victory

- **Chapter II - Taming Infinity with Generating Functions, Z-Transform**
     - The equations for our Infinit Grid of Resistors
     - The discrete laplacian
     - How to solve 1 dimensional difference equations
     - Example: Fibonacci Numbers
     - Powerful Tools: Generating Functions and Z-Transform
     - Trigonometric Functions as Solutions
     - The Characteristic Polynomial
     - Our Resistor Network in 1D

- **Chapter III - Finding a Solution for the Infinite Grid of Resistors with Fourier Series**
  The essence of the chapter is in this [jupiter notebook](notebooks/1ohm.ipynb):
     - Solutions of differential equations instead of difference equations. Why is it easier?
     - What is a Green's Function and why do we want one?
     - Constructing the Green's Function for our Equation
     - Calculating the Resistance from the Potential
     

     
- **Chapter IV - Solving the Integral**
     - Solving the Integral
     - Calculating a Resistance from its Neighbors
     

- **Chapter V (Bonus Chapter) - Numeric Solution with Python Numpy
     - Why Python?
     - Basics of Numpy
     - Monte Carlo Simulation of Calculating Pi in Plain Python and in Numpy
     - Relaxation Method for solving the Finite Grid
     - Calculating in only one Quadrant
     - Using Sparse Matrices (from scipy) to solve the Problem
     
Youtube Playlist: https://www.youtube.com/playlist?list=PLoGRr8ff1uXESrWh6z0BNTYpc4Y-hlBOm

If you are interested in a purely numerical simulation. I added a notebook
for that [notebook numerical simulation of finte grid](notebooks/resnetnum.ipynb).


[Links](Links.md)
